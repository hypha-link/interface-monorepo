// Tamagui CSS Reset (Disabled because it breaks styles & causes horizontal overflow)
// import '@tamagui/core/reset.css'
import '../styles/customTamaguiReset.css'
// Polyfill to provide animations
import 'raf/polyfill';
import React, { useMemo } from 'react';
import Head from 'next/head';
import '../styles/globalnextra.css';
import type { SolitoAppProps } from 'solito';
import { AppProvider } from 'app/provider';
import '../styles/fonts.css';
import { NextThemeProvider, useRootTheme } from '@tamagui/next-theme';

export default function MyApp({ Component, pageProps }: SolitoAppProps) {
  const [theme, setTheme] = useRootTheme()

  const contents = useMemo(() => {
    return <Component {...pageProps} />
  }, [pageProps])

	return (
    <>
      <Head>
        <title>Hypha</title>
        <meta name='description' content='Hypha Messaging' />
        <link rel='icon' href='../favicon.ico' />
      </Head>
      <NextThemeProvider
        onChangeTheme={(next) => {
          setTheme(next as any)
        }}
      >
        <AppProvider>
          {contents}
        </AppProvider>
      </NextThemeProvider>
		</>
	);
}
