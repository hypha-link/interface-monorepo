import React from 'react'
import styles from 'app/styles/address.module.css';
import { useRouter } from 'next/router'
import { isAddress } from 'ethers/lib/utils';
import { NavBar } from 'app/components/NavBar';
import Footer from 'app/components/Footer';
import ProfileCard from 'app/components/ProfileCard';
import Head from 'next/head';
import useProfile from 'app/components/hooks/useProfile';
import { SizableText, YStack } from '@hypha/ui';

export default function Address() {
  const router = useRouter();
  const { address } = router.query;
  const sAddress = typeof address  === 'string' ? address : '';

  const profile = useProfile(sAddress);

  return (
    isAddress(sAddress) ?
    <>
      <Head>
        <title>{profile?.name || sAddress} | Hypha</title>
        <meta name="description" content="Hypha Messaging" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar/>
      <YStack className={styles.address}>
        <ProfileCard profile={profile}/>
      </YStack>
      <Footer/>
    </>
    :
    <SizableText>404</SizableText>
  )
}
