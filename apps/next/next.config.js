/** @type {import('next').NextConfig} */
const { withTamagui } = require('@tamagui/next-plugin');
// const withNextra = require('nextra');
const { join } = require('path');

const boolVals = {
	true: true,
	false: false,
};

const disableExtraction =
	boolVals[process.env.DISABLE_EXTRACTION] ??
	process.env.NODE_ENV === 'development';

const plugins = [
	withTamagui({
		config: './tamagui.config.ts',
		components: ['tamagui', '@hypha/ui'],
		importsWhitelist: ['constants.js', 'colors.js'],
		logTimings: true,
		disableExtraction,
		// experiment - reduced bundle size react-native-web
		useReactNativeWebLite: false,
		shouldExtract: (path) => {
			if (path.includes(join('packages', 'app'))) {
				return true;
			}
		},
		excludeReactNativeWebExports: [
			'Switch',
			'ProgressBar',
			'Picker',
			'Modal',
			'VirtualizedList',
			'VirtualizedSectionList',
			'AnimatedFlatList',
			'FlatList',
			'CheckBox',
			'Touchable',
			'SectionList',
		],
	}),
	// withNextra({
	// 	theme: 'nextra-theme-docs',
	// 	themeConfig: './theme.config.tsx',
	// }),
];

module.exports = function () {
	/** @type {import('next').NextConfig} */
	let config = {
		typescript: {
			ignoreBuildErrors: true,
		},
		modularizeImports: {
			'@tamagui/lucide-icons': {
				transform: `@tamagui/lucide-icons/dist/esm/icons/{{kebabCase member}}`,
				skipDefaultConversion: true,
			},
		},
		trailingSlash: true,
		images: {
			domains: ['robohash.org', 'ipfs.io'],
		},
		transpilePackages: [
			'solito',
			'react-native-web',
			'expo-linking',
			'expo-constants',
			'expo-modules-core',
		],
		experimental: {
			appDir: false,
			// optimizeCss: true,
			scrollRestoration: true,
			legacyBrowsers: false,
		},
	};

	for (const plugin of plugins) {
		config = {
			...config,
			...plugin(config),
		};
	}

	return config;
};
