/* eslint-disable import/no-anonymous-default-export */
import React from 'react'
import { Logo } from '@hypha/ui/components'

export default {
  projectLink: 'https://gitlab.com/hypha-link/interface', // GitHub link in the navbar
  docsRepositoryBase: 'https://gitlab.com/hypha-link/interface/pages/docs', // base URL for the docs repository
  titleSuffix: ' | Hypha Docs',
  nextLinks: false,
  prevLinks: false,
  search: true,
  customSearch: null, // customizable, you can use algolia for example
  darkMode: true,
  font: false,
  footer: false,
  footerText: `MIT ${new Date().getFullYear()} © Hypha.`,
  footerEditLink: `Edit this page on GitLab`,
  logo: (
    <Logo.Hypha01 style={{height: '60', width: '60'}}/>
  ),
  head: (
    <>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="description" content="Documentation" />
      <meta name="og:title" content="Hypha" />
    </>
  ),
}