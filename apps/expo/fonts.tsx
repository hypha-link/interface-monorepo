import { useFonts } from "expo-font";
import { Montserrat, Alternates } from '@hypha/config/fonts'

export default function Fonts({ children }: { children: React.ReactNode }) {
    const [loaded, error] = useFonts({
        //MontserratAlternates
        MontserratThin: require('./assets/fonts/Montserrat/Montserrat-Thin.ttf'),
        MontserratExtraLight: require('./assets/fonts/Montserrat/Montserrat-ExtraLight.ttf'),
        MontserratLight: require('./assets/fonts/Montserrat/Montserrat-Light.ttf'),
        [Montserrat]: require('./assets/fonts/Montserrat/Montserrat-Regular.ttf'),
        MontserratMedium: require('./assets/fonts/Montserrat/Montserrat-Medium.ttf'),
        MontserratSemiBold: require('./assets/fonts/Montserrat/Montserrat-SemiBold.ttf'),
        MontserratBold: require('./assets/fonts/Montserrat/Montserrat-Bold.ttf'),
        MontserratExtraBold: require('./assets/fonts/Montserrat/Montserrat-ExtraBold.ttf'),
        MontserratBlack: require('./assets/fonts/Montserrat/Montserrat-Black.ttf'),
        //MontserratAlternates
        AlternatesThin: require('./assets/fonts/MontserratAlternates/MontserratAlternates-Thin.ttf'),
        AlternatesExtraLight: require('./assets/fonts/MontserratAlternates/MontserratAlternates-ExtraLight.ttf'),
        AlternatesLight: require('./assets/fonts/MontserratAlternates/MontserratAlternates-Light.ttf'),
        [Alternates]: require('./assets/fonts/MontserratAlternates/MontserratAlternates-Regular.ttf'),
        AlternatesMedium: require('./assets/fonts/MontserratAlternates/MontserratAlternates-Medium.ttf'),
        AlternatesSemiBold: require('./assets/fonts/MontserratAlternates/MontserratAlternates-SemiBold.ttf'),
        AlternatesBold: require('./assets/fonts/MontserratAlternates/MontserratAlternates-Bold.ttf'),
        AlternatesExtraBold: require('./assets/fonts/MontserratAlternates/MontserratAlternates-ExtraBold.ttf'),
        AlternatesBlack: require('./assets/fonts/MontserratAlternates/MontserratAlternates-Black.ttf'),
    })
    error && console.log(error);
    return <>{loaded && children}</>
  }