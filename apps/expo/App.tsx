import './expo-crypto-shim'
// Secure Random Number polyfill (Fixes missing strong random number source)
import 'react-native-get-random-values';
// Ethers JS shims polyfill (Fixes atob missing on RN)
// https://docs.ethers.io/v5/cookbook/react-native/
import '@ethersproject/shims';
//Supplies buffer required for wallet connect
import 'node-libs-react-native/globals.js';
// import 'url-polyfill';

import React from 'react';
import { NativeNavigation } from 'app/navigation/native';
import { AppProvider } from 'app/provider';
import Fonts from './fonts';
import { LogBox } from 'react-native';

LogBox.ignoreLogs([
	"The provided value 'ms-stream' is not a valid 'responseType'.",
	"The provided value 'moz-chunked-arraybuffer' is not a valid 'responseType'.",
]);

export default function App() {
	return (
		<AppProvider>
			<Fonts>
				<NativeNavigation />
			</Fonts>
		</AppProvider>
	);
}
