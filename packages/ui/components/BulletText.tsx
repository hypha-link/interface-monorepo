import { SizableText, SizableTextProps, XStack, YStack } from 'tamagui';
import React from 'react';

interface BulletTextProps extends SizableTextProps {
  children: React.ReactNode,
  bulletSize?: number,
  square?: boolean
}

export const BulletText:React.FC<BulletTextProps> = ({ bulletSize = 8, ...props }) => {
  return (
    <XStack
      // style={bulletTextStyles.container}
      style={{
        flexDirection: 'row', alignItems: 'center'
      }}
    >
      <YStack
        // style={bulletTextStyles.bullet}
        width={bulletSize}
        height={bulletSize}
        backgroundColor='$appColor'
        borderRadius={props.square ? 0 : 314}
      />
      <SizableText paddingLeft={bulletSize * 2} {...props}>
        {props.children}
      </SizableText>
    </XStack>
  );
};
