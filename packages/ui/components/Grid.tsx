import React from "react";
import { View, StyleProp, ViewStyle } from "react-native";

type GridProps = {
  children: React.ReactNode[], 
  columns: number, 
  gap?: number | string, 
  style?: StyleProp<ViewStyle>
}

export function HGrid({children, columns, gap, style} : GridProps) {

  const Row = ({ children }) => {
    return (
      <View
        style={{
          display: 'flex',
          flexDirection: 'row'
        }}
      >
        {children}
      </View>
    )
  }

  const Column = ({ children }) => {
    return (
      <View
        style={{
          display: 'flex',
          flex: 1, justifyContent: 'center',
          alignItems: 'center',
          margin: gap
        }}
      >
        {children}
      </View>
    )
  }

  /**
   * Loops through children to create rows & columns of children based on Grid columns prop
   * @returns Rows & Columns of children
   */
  const GridStructure = () => {
    let rows: React.ReactNode[] = [];
    let rowChildren: React.ReactNode[] = [];
    return children.map((item, index) => {
      const currentIndex = index + 1;
      // Reset row children for new row when row is full
      if(rowChildren.length === columns) rowChildren = [];
      // Create new row child
      rowChildren.push(
        <Column key={index}>{item}</Column>
      )
      // Create a new row when row is full
      if(currentIndex % columns === 0) {
        rows.push(
          <Row key={index}>{rowChildren}</Row>
        );
      }
      // Return row when full
      if(children.length - 1 === currentIndex) {
        return rows;
      }
    })
  }

    return (
        <View 
          style={{
            width: '100%',
            //Typed as object because of https://github.com/Microsoft/TypeScript/issues/10727. Remove later
            ...style as object
          }}
        >
          {GridStructure()}
        </View>
    )
  }