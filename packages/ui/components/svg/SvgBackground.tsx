import React from 'react'
import * as FA from './FA'
import * as Logo from './Logo'
import * as Patterns from './Patterns'
import * as Placeholder from './Placeholder'
import { ZStack, ZStackProps } from 'tamagui';
import { SvgProps } from './Svg'

export type allTypes = 
| 'Cog'
| 'Gitlab'
| 'Twitter'
| 'Youtube'
| 'Hypha01'
| 'Hypha03'
| 'Hypha04'
| 'Hypha05'
| 'HyphaTransparent'
| 'BlobScene'
| 'CircleScatter'
| 'HyphaPattern'
| 'LayeredSteps'
| 'LayeredWaves'
| 'LowPolyGrid'
| 'SymbolScatter'
| 'WaveHorizontal'
| 'WaveHorizontalBrown'
| 'WaveVertical'
| 'WaveVerticalBrown'
| 'QuestionMark' 
| 'UserPlaceholder'

interface SvgBackgroundProps extends ZStackProps {
    type: allTypes
    children?: React.ReactNode
}

export const SvgBackground: React.FC<SvgBackgroundProps> = props => {
    
    const styleDefault: SvgProps = {
        height: '100%',
        width: '100%',
    }

    function Select() {
        switch(props.type) {
            case 'Cog':
                return <FA.Cog {...styleDefault}/>
            case 'Gitlab':
                return <FA.Gitlab {...styleDefault}/>
            case 'Twitter':
                return <FA.Twitter {...styleDefault}/>
            case 'Youtube':
                return <FA.Youtube {...styleDefault}/>
            case 'Hypha01':
                return <Logo.Hypha01 {...styleDefault}/>
            case 'Hypha03':
                return <Logo.Hypha03 {...styleDefault}/>
            case 'Hypha04':
                return <Logo.Hypha04 {...styleDefault}/>
            case 'Hypha05':
                return <Logo.Hypha05 {...styleDefault}/>
            case 'HyphaTransparent':
                return <Logo.HyphaTransparent {...styleDefault}/>
            case 'BlobScene':
                return <Patterns.BlobScene {...styleDefault}/>
            case 'CircleScatter':
                return <Patterns.CircleScatter {...styleDefault}/>
            case 'HyphaPattern':
                return <Patterns.HyphaPattern {...styleDefault}/>
            case 'LayeredSteps':
                return <Patterns.LayeredSteps {...styleDefault}/>
            case 'LayeredWaves':
                return <Patterns.LayeredWaves {...styleDefault}/>
            case 'LowPolyGrid':
                return <Patterns.LowPolyGrid {...styleDefault}/>
            case 'SymbolScatter':
                return <Patterns.SymbolScatter {...styleDefault}/>
            case 'WaveHorizontal':
                return <Patterns.WaveHorizontal {...styleDefault}/>
            case 'WaveHorizontalBrown':
                return <Patterns.WaveHorizontalBrown {...styleDefault}/>
            case 'WaveVertical':
                return <Patterns.WaveVertical {...styleDefault}/>
            case 'WaveVerticalBrown':
                return <Patterns.WaveVerticalBrown {...styleDefault}/>
            case 'QuestionMark':
                return <Placeholder.QuestionMark {...styleDefault}/>
            case 'UserPlaceholder':
                return <Placeholder.UserPlaceholder {...styleDefault}/>
        }
    }

    return (
        <ZStack {...props}>
            {Select()}
            {props.children}
        </ZStack>
    )
}