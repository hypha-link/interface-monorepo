import { Svg as RNSvg, SvgProps as RNSvgProps } from 'react-native-svg'

export interface SvgProps extends RNSvgProps{
    children?: React.ReactNode
}

export function Svg({children, ...props} : SvgProps){
    return (
        <RNSvg
            {...props}
        >
            {children}
        </RNSvg>
    )
}