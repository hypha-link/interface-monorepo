export * from './Svg'
import * as FA from './FA'
import * as Logo from './Logo'
import * as Patterns from './Patterns'
import * as Placeholder from './Placeholder'
export * from './SvgBackground'

export { FA, Logo, Patterns, Placeholder }