import React, { useEffect, useState } from "react";
import { Button, YStack, YStackProps } from "tamagui";

type TooltipProps = {
  children: React.ReactNode
  content: string
  direction?: Direction
  delay?: number
}

type Direction =
  | "top"
  | "right"
  | "left"
  | "bottom"


export const HTooltip = ({ children, content, direction, delay } : TooltipProps) => {
//Tracks mouse entering/leaving container
const [active, setActive] = useState(false);
//Whether we display the tooltip
const [display, setDisplay] = useState(false);

  useEffect(() => {
    let timeout: ReturnType<typeof setTimeout>;
    if(active && content){
      timeout = setTimeout(() => {
        setDisplay(true);
      }, delay || 1200);
    }
    return () => {
      clearInterval(timeout);
      setDisplay(false);
    }
  }, [active, content, delay])

  const directions: {top: YStackProps, right: YStackProps, bottom: YStackProps, left: YStackProps} = {
    top: {
      // top: `calc(${sx({ top: 'tooltipMargin' })} * -1)`,
    },
    right: {
      // left: `calc(100% + ${sx({ top: 'tooltipMargin' })})`,
      top: '50%',
      // translate: '0, -50%',
    },
    bottom: {
      // bottom: `calc(${sx({ top: 'tooltipMargin' })} * -1)`,
    },
    left: {
      left: 'auto',
      // right: `calc(100% + ${sx({ top: 'tooltipMargin' })})`,
      top: '50%',
      // translate: '0, -50%',
    }
  }

  return (
    <Button
      // className={styles.tooltipWrapper}
      flexDirection='row'
      alignItems='center'
      justifyContent='center'
      // display='inline-block'
      position='relative'
      // When to show the tooltip
      onPressIn={() => setActive(true)}
      onPressOut={() => setActive(false)}
    >
      {/* Wrapping */}
      {children}
      {display && (
        <YStack 
          position='absolute'
          borderRadius={4}
          left='50%'
          // translate: '-50%, 0',
          padding={6}
          // color='uiColor'
          backgroundColor='uiBGColor'
          opacity={.8}
          // fontSize={14}
          // font-family: sans-serif,
          // line-height: 1,
          zIndex={100}
          // whiteSpace='nowrap'
          {...directions[direction] || directions.top}
        >
          {content}
        </YStack>
      )}
    </Button>
  );
};