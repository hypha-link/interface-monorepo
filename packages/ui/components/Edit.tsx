import React, { useContext, useState } from 'react'
import { StateContext } from 'app/context/state';
import { Input, XStack, XStackProps, getVariableValue, useTheme } from 'tamagui';
import { HButton, HButtonProps } from '..'

type InputType =
| 'name'
| 'description'

interface EditType extends XStackProps {
    type: InputType
}

export function Edit({type, ...style}: EditType){
    const [editing, setEditing] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const { selfId } = useContext(StateContext);
    const theme = useTheme()

    const keyHandler = (e) => {
        if (e.key === "Enter" && inputValue.trim() !== "") {
            selectedType(inputValue);
            setEditing(false);
            setInputValue("");
        }
    };
    
    const buttonHandler = (e) => {
        if (inputValue.trim() !== ""){
            selectedType(inputValue);
            setEditing(false);
            setInputValue("");
        }
    };

    const selectedType = (newChange) => {
        switch(type){
            case 'name':
                changeName(newChange);
                break;
            case 'description':
                changeDescription(newChange);
                break;
        }
    }

    const changeName = async (newName) => {
        console.log("Set Profile Name");
        await selfId.merge('basicProfile', {
            name: newName
        });
    }

    const changeDescription = async (newDescription) => {
        console.log("Set Profile Description");
        await selfId.merge('basicProfile', {
            description: newDescription
        });
    }

    const buttonStyle: HButtonProps = {
        marginLeft: '1%',
        // backgroundColor: `${getVariableValue(theme.uiHoverColor)}99`,
        borderRadius: 314,
        paddingHorizontal: '$3',
        pressStyle: {
            backgroundColor: '$appBGColor2',
            opacity: 1
        }
    }

    return (
        <XStack 
            flexDirection='row'
            alignItems='center'
            paddingLeft='3%'
            flex={1}
            {...style}
        >
            <HButton
                onPress={() => selfId && setEditing(!editing)}
                display={editing ? 'none' : 'flex'}
                {...buttonStyle}
            >
                <HButton.Text
                    color='$appColor'
                >
                    Edit
                </HButton.Text>
            </HButton>
            <XStack 
                display={!editing ? 'none' : 'flex'}
                flexDirection='row'
                alignItems='center'
                flex={1}
            >
                <Input
                    // name="edit"
                    // type="text"
                    placeholder={type}
                    autoComplete="off"
                    value={inputValue}
                    // onChange={(e) => setInputValue(e.target.value)}
                    onKeyPress={(e) => keyHandler(e)}
                    marginLeft='1%'
                    borderRadius={10}
                    // padding: '.4em .75em'
                    textAlign='center'
                />
                <HButton
                    onPress={(e) => buttonHandler(e)}
                    {...buttonStyle}
                >
                    <HButton.Text
                        color='$appColor'
                    >
                        Set
                    </HButton.Text>
                </HButton>
                <HButton
                    onPress={() => setEditing(!editing)}
                    {...buttonStyle}
                >
                    <HButton.Text
                        color='$appColor'
                    >
                        X
                    </HButton.Text>
                </HButton>
            </XStack>
        </XStack>
    )
}
