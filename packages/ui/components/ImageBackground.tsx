import React from 'react';
import { ImageSourcePropType } from 'react-native';
import { Image, ImageProps, ZStack, ZStackProps } from 'tamagui';

interface ImageBackgroundProps extends ZStackProps {
	source: ImageSourcePropType,
	imageStyle?: ImageProps,
	children?: React.ReactNode
}

export const ImageBackground: React.FC<ImageBackgroundProps> = props => {
	return (
		<ZStack 
			flex={1} 
			height='100%' 
			width='100%' 
			{...props}
		>
			<Image
				source={props.source}
				height='100%'
				width='100%'
				left={0}
				top={0}
				position='relative'
				{...props.imageStyle}
			/>
			{props.children}
		</ZStack>
	);
}
