import { styled, Input } from "tamagui"

export const HInput = styled(Input, {
    //Reset styles
    padding: 0,
    borderColor: 'transparent',
    borderWidth: 0,
    focusStyle:{
      borderWidth: 0,
      marginLeft: 0,
      marginRight: 0,
    },

    variants: {
        hypha: {
            standard: {
                color: '$pear',
                backgroundColor: '$cannon',
                borderWidth: 2,
                borderRadius: 5,
                hoverStyle: {
                    borderStyle: "solid",
                    borderColor: '#e6eff833',
                    backgroundColor: '$cannon',
                },
                focusStyle: {
                    borderStyle: "solid",
                    borderColor: '#e6eff833',
                    backgroundColor: '$cannon',
                }
            },
            unset: {
                padding: 0,
                paddingLeft: 0,
                paddingRight: 0,
                backgroundColor: 'transparent',
                borderColor: 'transparent',
                hoverStyle: {
                    backgroundColor: 'transparent',
                    borderColor: 'transparent'
                },
                focusStyle: {
                    backgroundColor: 'transparent',
                    borderColor: 'transparent'
                },
            },
        }
    } as const,

    defaultVariants: {
        hypha: 'unset',
    }
})