import React, { useContext } from 'react'
import { styled, GetProps, ButtonFrame, createStyledContext, SizeTokens, withStaticProperties, ButtonContext, SizableText } from "tamagui"

export const HButtonContext = createStyledContext({
    size: '$md' as SizeTokens,
})

export const HButtonFrame = styled(ButtonFrame, {
    name: 'HButton',
    context: HButtonContext,

    //Reset styles
    padding: 0,
    borderColor: 'transparent',

    variants: {
        hypha: {
            standard: {
                color: '$pear',
                backgroundColor: '$cannon',
                borderWidth: 2,
                borderRadius: 5,
                opacity: .6,
                hoverStyle: {
                    opacity: 1,
                    borderStyle: "solid",
                    borderColor: '#e6eff833',
                    backgroundColor: '$cannon',
                },
                focusStyle: {
                    opacity: 1,
                    borderStyle: "solid",
                    borderColor: '#e6eff833',
                    backgroundColor: '$cannon',
                }
            },
            clean: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: 0,
                paddingLeft: 0,
                paddingRight: 0,
                backgroundColor: 'transparent',
                borderColor: 'transparent',
                hoverStyle: {
                    backgroundColor: '$pear',
                    borderColor: 'transparent'
                },
                focusStyle: {
                    backgroundColor: 'transparent',
                    borderColor: 'transparent'
                },
            },
            unset: {
                padding: 0,
                paddingLeft: 0,
                paddingRight: 0,
                backgroundColor: 'transparent',
                borderColor: 'transparent',
                hoverStyle: {
                    backgroundColor: 'transparent',
                    borderColor: 'transparent'
                },
                focusStyle: {
                    backgroundColor: 'transparent',
                    borderColor: 'transparent'
                },
            },
        }
    } as const,

    defaultVariants: {
        hypha: 'unset',
    }
})

export const HButtonText = styled(SizableText, {
    name: 'ButtonText',
    context: HButtonContext,

    variants: {
        hypha: {
            standard: {
                color: '$appColor',
                margin: 0,
                padding: 0,
                fontSize: '$5',
                fontWeight: '$5',
                paddingHorizontal: '$4'
            }
        }
    } as const,
})

const HButtonIcon = (props: { children: React.ReactNode }) => {
    const { size } = useContext(HButtonContext)
}

export const HButton = withStaticProperties(HButtonFrame, {
    Text: HButtonText,
    Icon: HButtonIcon,
    Props: ButtonContext.Provider,
})

export type HButtonProps = GetProps<typeof HButtonFrame>