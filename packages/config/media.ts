import { createMedia } from '@tamagui/react-native-media-driver';

export const widths = [660, 800, 1020, 1200];

export const media = createMedia({
    xs: { maxWidth: 660 },
    gtXs: { minWidth: 660 + 1 },
    sm: { maxWidth: 860 },
    gtSm: { minWidth: 860 + 1 },
    md: { maxWidth: 980 },
    gtMd: { minWidth: 980 + 1 },
    lg: { maxWidth: 1120 },
    gtLg: { minWidth: 1120 + 1 },
    short: { maxHeight: 820 },
    tall: { minHeight: 820 },

	hoverNone: { hover: 'none' },
	pointerCoarse: { pointer: 'coarse' },
});
