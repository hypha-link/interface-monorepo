import { createTamagui, createTheme, createTokens } from 'tamagui'
import { shorthands } from '@tamagui/shorthands'
import { themes as tamaguiThemes, tokens as tamaguiTokens } from '@tamagui/themes'
import { MAFont, MFont } from './fonts'
import { media } from './media'
import { animations } from './animations'

const tokens = createTokens({
  ...tamaguiTokens,
  color: {
    olive: "#5B5F32",
    pear: "#D6DE23",
    cannon: "#2a1107",
    redstone1: "#FD627A",
    redstone2: "#24355b",
  },
  size: {
    // Sizing
    navHeight: 90,
    logoSize: 75,
    mobileLogoSize: 60,
    // Tooltip
    tooltipMargin: 30,
    tooltipArrowSize: 6,
    
    0: 0,
    1: 5,
    2: 10,
    3: 15,
    4: 20,
    5: 25,
    6: 30,
    7: 35,
    8: 40,
    9: 45,
    10: 50,
    11: 60,
    12: 70,
    13: 80,
    14: 90,
    15: 100,
    16: 120,
    17: 140,
    18: 160,
    19: 180,
    20: 200,
    21: 240,
    22: 280,
    23: 320,
    24: 360,
    25: 400,
    true: 50,
  },
  zIndex: {
    [-1]: -1,
    0: 0,
    1: 1,
    9: 9,
    50: 50,
    100: 100,
    king: 1000000
  },
  space: {
    0: 0,
    1: 4,
    2: 8,
    3: 16,
    4: 32,
    5: 64,
    6: 128,
    7: 256,
    8: 512,
    9: 1024,
    10: 2048,
    true: 32,
  },
  radius: {
    0: 0,
    1: 5
  },
})

const lightTheme = createTheme({
  appColor: tokens.color.olive,
  appBGColor: "white",
  appBGColor2: tokens.color.pear,

  uiColor: tokens.color.pear,
  uiBGColor: tokens.color.cannon,
  uiHoverColor: tokens.color.cannon,
  
  inputColor: tokens.color.pear,
  inputBGColor: tokens.color.olive,
  inputHoverColor: tokens.color.olive,
  inputHoverBGColor: tokens.color.pear,

  background: '#fff',
  backgroundHover: tokens.color.pear,
  backgroundPress: tokens.color.pear,
  backgroundFocus: tokens.color.pear,
  backgroundTransparent: tokens.color.pear,
  borderColor: tokens.color.pear,
  borderColorHover: tokens.color.pear,
  borderColorPress: tokens.color.pear,
  borderColorFocus: tokens.color.pear,
  color: tokens.color.olive,
  colorHover: tokens.color.pear,
  colorPress: tokens.color.pear,
  colorFocus: tokens.color.pear,
  shadowColor: tokens.color.pear,
  shadowColorHover: tokens.color.pear,
  shadowColorPress: tokens.color.pear,
  shadowColorFocus: tokens.color.pear,

})

type BaseTheme = typeof lightTheme

const darkTheme: BaseTheme = {
  appColor: tokens.color.pear,
  appBGColor: "black",
  appBGColor2: tokens.color.olive,
  uiColor: tokens.color.olive,
  uiBGColor: tokens.color.cannon,
  uiHoverColor: tokens.color.cannon,
  inputColor: tokens.color.olive,
  inputBGColor: tokens.color.pear,
  inputHoverColor: tokens.color.pear,
  inputHoverBGColor: tokens.color.olive,

  background: '#fff',
  backgroundHover: tokens.color.olive,
  backgroundPress: tokens.color.olive,
  backgroundFocus: tokens.color.olive,
  backgroundTransparent: tokens.color.olive,
  borderColor: tokens.color.olive,
  borderColorHover: tokens.color.olive,
  borderColorPress: tokens.color.olive,
  borderColorFocus: tokens.color.olive,
  color: tokens.color.olive,
  colorHover: tokens.color.olive,
  colorPress: tokens.color.olive,
  colorFocus: tokens.color.olive,
  shadowColor: tokens.color.olive,
  shadowColorHover: tokens.color.olive,
  shadowColorPress: tokens.color.olive,
  shadowColorFocus: tokens.color.olive,
}

const allThemes = {
  lightTheme,
  darkTheme,
}

type theme = keyof typeof allThemes

type Themes = {
  [key in theme]: BaseTheme
}

export const themes: Themes = {...allThemes, ...tamaguiThemes}

export const config = createTamagui({
  defaultFont: 'body',
	tokens,
	themes,
  media,
	fonts: {
		heading: MAFont,
		body: MFont,
		montserrat: MFont,
		alternates: MAFont,
	},
	shorthands,
	animations,
});