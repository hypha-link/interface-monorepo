import { createFont } from 'tamagui'
import { Platform } from 'react-native'

export const Montserrat = "Montserrat";
export const Alternates = "Alternates";

const size = {
    0: 8,
    1: 10,
    2: 12,
    3: 16,
    4: 20,
    5: 24,
    6: 32,
    7: 40,
    8: 48,
    true: 16,
}

const letterSpacing = {
    4: 0,
    6: -1
}

const lineHeight = {
    1: 17,
    2: 22,
    3: 25,  
}

const weight = {
    1: '100',
    2: '200',
    3: '300',
    4: '400',
    5: '500',
    6: '600',
    7: '700', 
    8: '800',
    9: '900',
}

export const MFont = createFont(
    {
        face: {
            100: { normal: `${Montserrat}Thin` },
            200: { normal: `${Montserrat}ExtraLight` },
            300: { normal: `${Montserrat}Light` },
            400: { normal: `${Montserrat}`, italic: `${Montserrat}Italic` },
            500: { normal: `${Montserrat}Medium` },
            600: { normal: `${Montserrat}SemiBold` },
            700: { normal: `${Montserrat}Bold` },
            800: { normal: `${Montserrat}ExtraBold` },
            900: { normal: `${Montserrat}Black` },
        },
        size,
        family: Platform.OS === 'web' ? `${Montserrat}, Helvetica, Arial, sans-serif` : Montserrat,
        letterSpacing,
        lineHeight,
        weight
    }
)
  
export const MAFont = createFont(
    {
        face: {
            100: { normal: `${Alternates}Thin` },
            200: { normal: `${Alternates}ExtraLight` },
            300: { normal: `${Alternates}Light` },
            400: { normal: `${Alternates}`, italic: `${Alternates}Italic` },
            500: { normal: `${Alternates}Medium` },
            600: { normal: `${Alternates}SemiBold` },
            700: { normal: `${Alternates}Bold` },
            800: { normal: `${Alternates}ExtraBold` },
            900: { normal: `${Alternates}Black` },
        },
        size,
        family: Platform.OS === 'web' ? `${Alternates}, Helvetica, Arial, sans-serif` : Alternates,
        letterSpacing,
        lineHeight,
        weight,
    }
)

export const AltThin = createFont(
    {
        face: {
            100: { normal: `${Alternates}Thin` },
            200: { normal: `${Alternates}ExtraLight` },
            300: { normal: `${Alternates}Light` },
            400: { normal: `${Alternates}`, italic: `${Alternates}Italic` },
            500: { normal: `${Alternates}Medium` },
            600: { normal: `${Alternates}SemiBold` },
            700: { normal: `${Alternates}Bold` },
            800: { normal: `${Alternates}ExtraBold` },
            900: { normal: `${Alternates}Black` },
        },
        size,
        family: Platform.OS === 'web' ? `${Alternates}Thin, Helvetica, Arial, sans-serif` : Alternates,
        letterSpacing,
        lineHeight,
        weight,
    }
)