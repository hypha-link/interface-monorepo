import { createContext, ReactElement } from "react"

export const OcclusionContext = createContext<ReactElement>(null)
export const OcclusionUpdateContext = createContext<React.Dispatch<ReactElement>>(() => null)