import { createContext } from "react";
import { GlobalState, GlobalDispatch } from 'app/provider/state/AppContextTypes'
import { initialState } from "app/provider/state/reducer";

export const StateContext = createContext<GlobalState>(initialState);
export const DispatchContext = createContext<GlobalDispatch>(() => {});