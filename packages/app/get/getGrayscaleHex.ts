import colorsea from 'colorsea'

export default function getGrayscaleHex(value: any) {
    // Calculate equivalent grayscale hex
    const rgba = colorsea(value).rgba()
    const gray = (0.299 * rgba[0] + 0.587 * rgba[1] + 0.114 * rgba[2]);
    const final = colorsea.rgb(gray, gray, gray, rgba[3])
    return final.hex()
}
