import React, { useState, useRef } from "react";
import { HButton, HButtonProps, Sheet, SizableTextProps, Spacer, YStack } from "@hypha/ui";
import { Conversations } from "./utils/Types";
import ConversationComponent from "./conversations/ConversationComponent";

type ContextMenuProps = {
  children: React.ReactNode
  options: {name: string , fn: (() => void)}[]
  conversation?: Conversations
  onPress?: () => void
}

/**
 * Creates a context menu
 * @param options Array of options available on the context menu
 * @param conversation The origin conversation
 * @param onPress Function to call when ContextMenu button is pressed
 */
export default function ContextMenu({ children, options, conversation, onPress } : ContextMenuProps){
  const childRef = useRef<Element>();

  const childElements = React.Children.map(children, child => {
    //@ts-ignore
    if(React.isValidElement(child)) return React.cloneElement(child, { ref: childRef })
  })

  const optionIcons = new Map<string, string>();
  optionIcons.set("select", "➢");
  optionIcons.set("view", "⌗")
  optionIcons.set("invite", "⁂")
  optionIcons.set("send", "🡽");
  optionIcons.set("copy", "❐");
  optionIcons.set("delete", "X");

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [position, setPosition] = useState<number>(0)

  interface optionsComponentTypes extends HButtonProps{
    option: {name: string, fn: () => void}
    last: boolean
  }
  
  /**
   * @param last Whether this is the last option to be rendered
   */
  function OptionComponent({option, last, ...style}: optionsComponentTypes){
    const [press, setPress] = useState(false);
    
    const styles: { container: HButtonProps, text: SizableTextProps, lastContainer: HButtonProps, lastText: SizableTextProps} = {
      container: {
        borderBottomColor: '$appColor',
        borderBottomWidth: 1,
      },
      text: {
        margin: 0,
        padding: 0,
        color: '$inputHoverColor',
        fontSize: '$5',
        fontWeight: '$5',
        paddingHorizontal: '$4'
      },
      lastContainer: {

      },
      lastText: {
        color: 'red',
        flex: 1,
        textAlign: 'center'
      }
    }
    return (
      <HButton 
        // hypha="unset"
        key={option.name}
        onPress={(e) => {
          e.stopPropagation();
          option.fn();
          setOpenModal(false);
        }}
        onPressIn={() => setPress(true)}
        onPressOut={() => setPress(false)}
        flexDirection='row'
        flex={1}
        jc="center"
        ai="center"
        width='100%'
        backgroundColor='transparent'
        opacity={press ? 1 : .6}
        {...style}
        {...!last ? {...styles.container} : {}}
      >
        {/* {
          !last ?
          <SizableText flex={.7} textAlign="right" {...styles.text} {...last ? {...styles.lastText} : {}} >
            {optionIcons.get(option.name)}
          </SizableText>
          :
          null
        } */}
        <HButton.Text flex={1} {...styles.text} textAlign="center" {...last ? {...styles.lastText} : {}}>
          {option.name.replace('_', ' ')}
        </HButton.Text>
      </HButton>
    )
  }

  return(
    <>
      <Sheet
        modal
        open={openModal}
        onOpenChange={setOpenModal}
        snapPoints={[40]}
        position={position}
        onPositionChange={setPosition}
        dismissOnSnapToBottom
      >
        <Sheet.Overlay backgroundColor="#00000090"/>
        <Sheet.Frame ai="center" jc="center" backgroundColor='$appBGColor'>
          <YStack
            position='absolute'
            width='100%'
            zIndex='$9'
          >
            <ConversationComponent conversation={conversation} align="center" />
            <Spacer size='$3' />
            {
              options.map((option, index) => {
                return(
                  <OptionComponent key={option.name} option={option} last={index === options.length - 1} />
                )
              })
            }
          </YStack>
        </Sheet.Frame>
      </Sheet>
      <HButton
        flexDirection='row'
        width='100%'
        onPress={() => onPress()}
        onLongPress={() => setOpenModal(!openModal)}
      >
        {childElements}
      </HButton>
    </>
  )
};