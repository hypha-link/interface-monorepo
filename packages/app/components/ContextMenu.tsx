import React, { useState, useCallback, useEffect, useRef } from "react";
import { HButton, HButtonProps, SizableTextProps, YStack } from "@hypha/ui";
import { Conversations } from "./utils/Types";

type ContextMenuProps = {
  children: React.ReactNode
  options: {name: string , fn: (() => void)}[]
  conversation?: Conversations
  onPress?: () => void
}

/**
 * Creates a context menu
 * @param options Array of options available on the context menu
 * @param conversation The origin conversation
 * @param onPress Function to call when ContextMenu button is pressed
 */
export default function ContextMenu({ children, options, conversation, onPress } : ContextMenuProps){
  const [anchorPoint, setAnchorPoint] = useState({x: 0, y: 0});
  const display = anchorPoint.x !== 0 && anchorPoint.y !== 0;
  const mouseCoords = useRef({x: 0, y: 0});
  const childRef = useRef<Element>();

  const childElements = React.Children.map(children, child => {
    //@ts-ignore
    if(React.isValidElement(child)) return React.cloneElement(child, { ref: childRef })
  })

  const handleClick = useCallback((e: Event) => {
    if(display)
      setAnchorPoint({ x: 0, y: 0 });
  }, [display]);

  const handleContext = useCallback((e: Event) => {
    const childBounds = childRef.current.getBoundingClientRect();
    const mouseInsideChild = mouseCoords.current.x > childBounds.left && mouseCoords.current.x < childBounds.right && mouseCoords.current.y > childBounds.top && mouseCoords.current.y < childBounds.bottom;
    if(display && !mouseInsideChild){
      e.preventDefault();
      setAnchorPoint({ x: 0, y: 0 });
    }
    else{
      if(mouseInsideChild){
        e.preventDefault();
        setAnchorPoint(mouseCoords.current);
      }
    }
  }, [display]);

  const handleMouseMove = (e: MouseEvent) => {
    mouseCoords.current = {
       x: e.clientX, 
       y: e.clientY 
      }
  }

  useEffect(() => {
    document.addEventListener("click", handleClick);
    document.addEventListener("contextmenu", handleContext);
    document.addEventListener("mousemove", handleMouseMove);
    return () => {
      document.removeEventListener("click", handleClick);
      document.removeEventListener("contextmenu", handleContext);
      document.removeEventListener("mousemove", handleMouseMove);
    };
  });

  const optionIcons = new Map<string, string>();
  optionIcons.set("select", "➢");
  optionIcons.set("view", "⌗")
  optionIcons.set("invite", "⁂")
  optionIcons.set("send", "🡽");
  optionIcons.set("copy", "❐");
  optionIcons.set("delete", "X");

  interface optionsComponentTypes extends HButtonProps{
    option: {name: string, fn: () => void}
    last: boolean
  }
  
  /**
   * @param last Whether this is the last option to be rendered
   */
  function OptionComponent({option, last, ...style}: optionsComponentTypes){
    const [hover, setHover] = useState(false);

    const styles: { container: HButtonProps, text: SizableTextProps, lastContainer: HButtonProps, lastText: SizableTextProps} = {
      container: {
        borderBottomColor: '$appBGColor',
        borderBottomWidth: 1,
      },
      text: {
        margin: 0,
        padding: 0,
        color: hover ? '$appColor' : '$appBGColor',
        fontSize: '$3',
        fontWeight: '$6',
      },
      lastContainer: {

      },
      lastText: {
        color: 'red',
        flex: 1,
        textAlign: 'center'
      }
    }
    return (
      <HButton 
        hypha="clean"
        // For icons
        flexDirection='row'
        flex={1}
        width='100%'
        {...style}
        {...!last ? {...styles.container} : {}}
        onPress={(e) => {
          e.stopPropagation();
          option.fn();
          setAnchorPoint({ x: 0, y: 0 });
        }}
        onHoverIn={() => setHover(true)}
        onHoverOut={() => setHover(false)}
      >
        {/* {
          !last ?
          <SizableText flex={.7} textAlign="right" {...styles.text} {...last ? {...styles.lastText} : {}} >
            {optionIcons.get(option.name)}
          </SizableText>
          :
          null
        } */}
        <HButton.Text flex={1} {...styles.text} textAlign="center" {...last ? {...styles.lastText} : {}}>
          {option.name.replace('_', ' ')}
        </HButton.Text>
      </HButton>
    )
  }

  return(
    display ?
    <>
      <YStack
        display='flex'
        flexDirection='column'
        position='absolute'
        backgroundColor='$uiBGColor'
        borderRadius={10}
        width={'$22'}
        paddingVertical={6}
        paddingHorizontal={0}
        zIndex={10}
        left={anchorPoint.x}
        right={anchorPoint.y}
      >
        {
          options.map((option, index) => {
            return(
              <OptionComponent key={option.name} option={option} last={index === options.length - 1} />
            )
          })
        }
      </YStack>
      <HButton
        flexDirection='row'
        width='100%'
        onPress={() => onPress()} 
      >
        {childElements}
      </HButton>
    </>
    
    :

    <HButton
      flexDirection='row'
      width='100%'
      onPress={() => onPress()} 
    >
      {childElements}
    </HButton>
  )
};