import Link from 'next/link'
import React from 'react'
import { useMedia } from 'tamagui';

interface links {
    name: string,
    description: string,
    url: string,
}

export function Feature({ links }: { links: links[] }) {
    const { sm } = useMedia();
    const gridStyle = sm ?
        {
            display: 'grid',
            gridTemplateColumns: '1fr',
            gridTemplateRows: '1fr',
            gap: 16,
            margin: '5%',
        }
        :
        {
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            gridTemplateRows: '1fr 1fr',
            gap: 16,
            margin: '5%',
        }
    return (
        // className={styles.docsUtils}
        <div 
            className='docsUtils'
            style={gridStyle}
        >
            {links.map(link => {
                return (
                    <Link
                        key={link.name}
                        href={`/docs${link.url}`}
                        // target={'_blank'}
                        // rel={'noreferrer'}
                    >
                        <a 
                            style={{
                                textDecoration: 'none',
                                backgroundColor: '#1a202c',
                                color: 'white',
                                borderRadius: 16,
                                /* box-shadow: -.5rem -.5rem var(--cannon) inset; */
                                transition: 'box-shadow 0.3s ease-in-out',
                            }}
                        >
                            <div
                                style={{
                                    position: 'relative',
                                    padding: '5%',
                                }}
                            >
                            {/* className={styles.docsUtilsArrow} */}
                                <p 
                                    style={{
                                        ...{
                                            margin: 0
                                        }, 
                                        ...{
                                            position: 'absolute',
                                            top: '1vh',
                                            right: '1vw',
                                            color: 'gray',
                                        },
                                    }}
                                >
                                    🡵
                                </p>
                                <h6>{link.name}</h6>
                                <LightText>{link.description}</LightText>
                            </div>
                        </a>
                    </Link>
                )
            })}
        </div>
    )
}

export function LightText({ children }: {children: React.ReactNode }){
    return(
        <p 
            style={{color: 'gray'}}
        >
            {children}
        </p>
    )
}