import React, { useContext, useEffect, useState } from "react";
// import styles from '../styles/message.module.css';
import { TokenFeed } from "./TokenFeed";
import ContextMenu from "./ContextMenu";
import { MessagePayload } from "./utils/Types";
import { StateContext } from "app/context/state";
import useSelectedConversation from "./hooks/useSelectedConversation";
import ReactMarkdown from "react-markdown";
import remarkGfm from "remark-gfm";
import remarkEmoji from "remark-emoji";
import remarkImages from "remark-images";
import ProfileImage from "./ProfileImage";
import { getVariableValue, SizableText, SizableTextProps, useTheme, YStack, YStackProps } from "@hypha/ui";
import { merge } from "lodash"

type MessageProps = {
  payload: MessagePayload
  selectMessage: (payload: MessagePayload) => void
  deleteMessage: (payload: MessagePayload) => void
}

export function Message({payload, selectMessage, deleteMessage}: MessageProps) {
  const { message, sender, date } = payload;
  const theme = useTheme();
  const selectedConversation = useSelectedConversation();
  const { ownProfile } = useContext(StateContext);
  //Message that has been edited prior to markdown modification
  const [editedMessage, setEditedMessage] = useState(message);

  //Set to owner profile, otherwise set to address that matches profile
  const profile = sender === ownProfile.address ? ownProfile : selectedConversation.profile.find(_profile => _profile.address === sender);
  //Regex that finds all price feeds in the message
  const priceFeedRegex = /\[[a-zA-Z0-9]+,[0-9]*\.[0-9]+\]/g;
  //Price feeds array
  const tokenFeedArr: JSX.Element[] = [];

  //Check for price feed
  // if(message.match(priceFeedRegex)){
  //   message.match(priceFeedRegex).forEach(feed => {
  //     //Add the feed to message
  //     tokenFeedArr.push(
  //       <TokenFeed
  //         key={Math.random()}
  //         tokenName={feed.substring(1, feed.indexOf(","))}
  //         //+ Converts string to number
  //         tokenPrice={+feed.substring(feed.indexOf(",") + 1, feed.indexOf("]"))}
  //         hideLiveFeedCheckbox={false}
  //         onClick={() => console.log(message)}
  //       />
  //     )
  //   });
  // }

  //Remove feed shortcode from the message
  // useEffect(() => setEditedMessage(message.replaceAll(priceFeedRegex, '')), [tokenFeedArr]);

  const styles: {container: YStackProps, childContainer: YStackProps, child: YStackProps, messageID: SizableTextProps, messageDate: SizableTextProps, messageText: SizableTextProps} = {
    container: {
      flexDirection: 'row',
      alignItems: 'flex-start',
      padding: '2.5%',
      borderRadius: 5,
      hoverStyle: {
        backgroundColor: `${getVariableValue(theme.uiHoverColor)}05`,
      }
    },
    // .message div img {
    //   border-radius: 5px;
    //   max-height: 20vh;
    //   max-width: 20vw;
    // }
    childContainer: {
      flexDirection: 'column',
      flex: 1,
      marginVertical: 0,
      marginHorizontal: '2%',
    },
    child: {
      justifyContent: 'flex-start',
      alignItems: 'center',
      marginBottom: '$3',
      // whiteSpace: 'nowrap',
    },
    messageID: {
      marginRight: '$3'
    },
    messageDate: {
      fontSize: '$2',
      fontStyle: 'italic',
    },
    messageText: {
      // wordBreak: 'break-word',
    }
  }

  const own: typeof styles = {
    container: {
      flexDirection: 'row-reverse',
    },
    childContainer: {
      alignItems: 'flex-end',
    },
    child: {
      flexDirection: 'row-reverse',
    },
    messageID: {
      marginRight: '2%',
    },
    messageDate: {
      marginRight: '2%',
    },
    messageText: {
      marginRight: '2%',
    },
  }

  // Deep merge styles & own using lodash
  const finalStyles = sender === profile.address ? merge(styles, own) : styles;

  return (
    // <ContextMenu
    //   options={[
    //     {name: 'copy', fn: async () => navigator.clipboard.writeText(message)},
    //     {name: 'delete', fn: () => deleteMessage(payload)},
    //   ]}
    // >
      <YStack
        {...finalStyles.container}
        // className={sender === profile.address ? `${styles.message} ${styles.own}` : styles.message}
        onClick={() => selectMessage(payload)}
      >
        <ProfileImage profile={profile} sizePx={75}/>
        <YStack 
          {...finalStyles.childContainer}
        >
          <YStack 
            {...finalStyles.child}
          >
            {/* id={styles.messageID} */}
            <SizableText 
              {...finalStyles.messageID}
            >
              {profile?.name ? profile.name : sender}
            </SizableText>
            {/* id={styles.messageDate} */}
            <SizableText 
              {...finalStyles.messageDate}
            >
              {date}
            </SizableText>
          </YStack>
          {/* Message Content */}
          {tokenFeedArr}
          <ReactMarkdown
            remarkPlugins={[[remarkGfm], [remarkEmoji, {emoticon: true}], [remarkImages]]}
            linkTarget={"_blank"}
          >
            {editedMessage}
          </ReactMarkdown>
        </YStack>
      </YStack>
    // </ContextMenu>
  );
}
