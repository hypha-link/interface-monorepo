import React, { useContext } from 'react'
import { StateContext } from 'app/context/state';
import ProfileImage from './ProfileImage';
import getShortAddress from '../get/getShortAddress';
import useWeb3 from './hooks/useWeb3';
import { HButtonProps, HButton, SizableText, SizableTextProps, YStack } from '@hypha/ui';

export const ConnectButton = () => {
  const { connect, disconnect, connected } = useWeb3();
  const { ownProfile } = useContext(StateContext);

  const connectButtonStyles: {container: HButtonProps, text: SizableTextProps} = {
    container: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: '100%',
      minWidth: 100,
      height: 50,
      borderRadius: 314,
      backgroundColor: '$uiBGColor',
      opacity: .6,
      padding: 0,
      borderColor: connected ? '$appBGColor2' : 'transparent',
      hoverStyle: {
        backgroundColor: '$uiHoverColor',
        borderColor: connected ? '$appBGColor2' : 'transparent',
      }
    },
    text: {
      flexGrow: 1,
      marginVertical: 0,
      marginHorizontal: 10,
      fontSize: '$3',
      color: '$uiColor',
      textAlign: 'center',
      userSelect: 'none',
    }
  }

  return (
    <YStack>
      {
      connected ?
        <HButton 
          hypha="standard"
          onPress={async () => {
            await disconnect();
          }} 
          {...connectButtonStyles.container}
        >
          <ProfileImage profile={ownProfile}/>
          <SizableText 
            {...connectButtonStyles.text}
          >
            {ownProfile?.name ? ownProfile?.name : getShortAddress(ownProfile?.address)}
          </SizableText>
        </HButton>
      :
        <HButton 
          hypha="standard"
          onPress={async () => {
            await connect();
          }} 
          {...connectButtonStyles.container}
        >
          <SizableText 
            {...connectButtonStyles.text}
          >
            Connect
          </SizableText>
        </HButton>
      }
    </YStack>
  );
};