import { utils } from 'ethers';
import React, { useContext, useState } from 'react'
import { StateContext } from 'app/context/state';
import {
    Dialog,
    Fieldset,
    HButton,
    HInput,
    Label,
    SizableText,
    XStack,
    YStack
} from '@hypha/ui';
import { useConversations } from 'app/services/conversations';

export const ConversationModal = () => {
    const [inputValue, setInputValue] = useState('');
    const [placeholderText, setPlaceholderText] = useState('');
    const { ownProfile } = useContext(StateContext);
    const Orchestrator = useConversations();

    const keyHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Enter") {
            handleSubmission();
        }
    };

    const handleSubmission = () => {
        if (utils.isAddress(inputValue) && inputValue !== ownProfile?.address) {
            addConversation(inputValue);
        }
        else if (inputValue === '') {
            setPlaceholderText('Enter an Ethereum address');
            setInputValue('');
        }
        else if (inputValue === ownProfile?.address) {
            setPlaceholderText('Please enter the address of another user');
            setInputValue('');
        }
        else {
            setPlaceholderText(`${inputValue} is not an Ethereum address`);
            setInputValue('');
        }
    }

    const addConversation = (address: string) => {
        // addConversations(address);
        Orchestrator({ type: 'ADD_CONVERSATION', payload: address })
        console.log('adding conversation')
    }

    return (
        <Dialog
            onOpenChange={(open) => {
                if (!open) {
                    setInputValue('');
                }
            }}
        >
            <Dialog.Trigger asChild>
                <HButton
                    hypha='standard'
                    // onPress={() => {setConversationModal('')}}
                    // disabled={!selfId}
                    width='75%'
                    margin='2%'
                    padding='2%'
                    size={60}
                >
                    <HButton.Text
                        color={'$inputColor'}
                        size={60}
                        userSelect='none'
                    >
                        Add Conversations
                    </HButton.Text>
                </HButton>
            </Dialog.Trigger>
            <Dialog.Portal>
                <Dialog.Overlay
                    key="overlay"
                    animation="quick"
                    backgroundColor='#00000090'
                    enterStyle={{ opacity: 0 }}
                    exitStyle={{ opacity: 0 }}
                />
                <Dialog.Content
                    elevate
                    key="content"
                    animation={[
                        'quick',
                        {
                            opacity: {
                                overshootClamping: true,
                            },
                        },
                    ]}
                    enterStyle={{ x: 0, y: -20, opacity: 0, scale: 0.9 }}
                    exitStyle={{ x: 0, y: 10, opacity: 0, scale: 0.95 }}
                    x={0}
                    scale={1}
                    opacity={1}
                    y={0}
                    shadowColor='transparent'
                >
                    <YStack space width='$25'>
                        <Dialog.Title
                            textAlign='center'
                            fontSize='$5'
                            fontWeight='$5'
                        >
                            Add Conversations
                        </Dialog.Title>
                        <Fieldset ai='center' borderColor='transparent'>
                            <Label
                                htmlFor='address'
                                fontSize='$4'
                                fontWeight='$4'
                            >
                                Ethereum Address
                            </Label>
                            <HInput
                                id='address'
                                placeholder={placeholderText}
                                value={inputValue}
                                width='100%'
                                fontSize='$3'
                                fontWeight='$6'
                                borderWidth={1}
                                borderColor='$appColor'
                                borderStyle='solid'
                                focusStyle={{
                                    borderWidth: 1,
                                    borderColor: '$appColor',
                                    borderStyle: 'solid',
                                    outlineColor: 'transparent'
                                }}
                                hoverStyle={{
                                    borderWidth: 1,
                                    borderColor: '$appColor',
                                    borderStyle: 'solid',
                                }}
                                paddingHorizontal='$2'
                                onChange={(e) => setInputValue(e.nativeEvent.text)}
                            // onKeyPress={(e) => keyHandler(e.nativeEvent.key)}
                            />
                        </Fieldset>
                        <XStack justifyContent='space-around'>
                            <HButton
                                hypha='clean'
                                flexBasis='50%'
                                borderRadius='$1'
                                onPress={() => handleSubmission()}
                            >
                                <HButton.Text
                                    fontSize='$5'
                                    fontWeight='$5'
                                >
                                    Add
                                </HButton.Text>
                            </HButton>
                            <Dialog.Close asChild>
                                <HButton
                                    hypha='clean'
                                    flexBasis='50%'
                                    borderRadius='$1'
                                >
                                    <HButton.Text
                                        fontSize='$5'
                                        fontWeight='$5'
                                    >
                                        Close
                                    </HButton.Text>
                                </HButton>
                            </Dialog.Close>
                        </XStack>
                    </YStack>
                </Dialog.Content>
            </Dialog.Portal>
        </Dialog>
    )
}