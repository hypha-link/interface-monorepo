import { utils } from 'ethers';
import React, { useContext, useState } from 'react'
import { StateContext } from 'app/context/state';
import {
    Fieldset,
    HButton,
    HInput,
    Label,
    Sheet,
    SizableText,
    XStack,
    YStack,
} from '@hypha/ui';
import { useConversations } from 'app/services/conversations';

export const ConversationModal = () => {
    const [inputValue, setInputValue] = useState('');
    const [placeholderText, setPlaceholderText] = useState('');
    const { ownProfile } = useContext(StateContext);
    const [openModal, setOpenModal] = useState<boolean>(undefined);
    const [position, setPosition] = useState<number>(0)
    const Orchestrator = useConversations();

    const keyHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Enter") {
            handleSubmission();
        }
    };

    const handleSubmission = () => {
        if (utils.isAddress(inputValue) && inputValue !== ownProfile?.address) {
            addConversation(inputValue);
        }
        else if (inputValue === '') {
            setPlaceholderText('Enter an Ethereum address');
            setInputValue('');
        }
        else if (inputValue === ownProfile?.address) {
            setPlaceholderText('Please enter the address of another user');
            setInputValue('');
        }
        else {
            setPlaceholderText(`${inputValue} is not an Ethereum address`);
            setInputValue('');
        }
    }

    const addConversation = (address: string) => {
        setOpenModal(false);
        Orchestrator({ type: 'ADD_CONVERSATION', payload: address })
        console.log('adding conversation')
    }

    return (
        <>
            <HButton
                hypha="standard"
                onPress={() => setOpenModal(true)}
                // disabled={!selfId}
                margin='2%'
                padding='2%'
                size={60}
            >
                <SizableText
                    color={'$inputColor'}
                    size={60}
                    userSelect='none'
                >
                    Add Conversations
                </SizableText>
            </HButton>
            <Sheet
                modal
                open={openModal}
                onOpenChange={setOpenModal}
                snapPoints={[40]}
                position={position}
                onPositionChange={setPosition}
                dismissOnSnapToBottom
            >
                <Sheet.Overlay backgroundColor="#00000090" />
                <Sheet.Frame backgroundColor='$appBGColor'>
                    <YStack
                        flexBasis='100%'
                        justifyContent='space-between'
                        paddingVertical='$4'
                    >
                        <SizableText
                            color='$appColor'
                            fontSize='$5'
                            fontWeight='$5'
                            textAlign='center'
                        >
                            Add conversation?
                        </SizableText>
                        <Fieldset ai='center'>
                            <Label
                                htmlFor='address'
                                color='$appColor'
                                fontSize='$4'
                                fontWeight='$4'
                            >
                                Ethereum Address
                            </Label>
                            <HInput
                                id='address'
                                placeholder={placeholderText}
                                placeholderTextColor='$appColor'
                                value={inputValue}
                                onChange={(e) => setInputValue(e.nativeEvent.text)}
                                // onKeyPress={(e) => keyHandler(e.nativeEvent.key)}
                                textAlign='center'
                                width='75%'
                                borderWidth={1}
                                borderColor='$appColor'
                                borderStyle='solid'
                                color='$appColor'
                                backgroundColor='transparent'
                                flexWrap='wrap'
                                fontSize='$3'
                                fontWeight='$6'
                                focusStyle={{
                                    borderWidth: 1,
                                    borderColor: '$appColor',
                                    borderStyle: 'solid'
                                }}
                            />
                        </Fieldset>
                        <XStack justifyContent='space-around'>
                            <HButton
                                hypha='clean'
                                flexBasis='50%'
                                borderRadius='$1'
                                onPress={() => handleSubmission()}
                            >
                                <HButton.Text
                                    fontSize='$5'
                                    fontWeight='$5'
                                >
                                    Add
                                </HButton.Text>
                            </HButton>
                            <HButton
                                hypha='clean'
                                flexBasis='50%'
                                borderRadius='$1'
                                onPress={() => {
                                    setOpenModal(false);
                                    setInputValue('');
                                }}
                            >
                                <HButton.Text
                                    fontSize='$5'
                                    fontWeight='$5'
                                >
                                    Close
                                </HButton.Text>
                            </HButton>
                        </XStack>
                    </YStack>
                </Sheet.Frame>
            </Sheet>
        </>
    )
}
