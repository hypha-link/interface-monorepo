import { YStack, SizableText, HButton, YStackProps, HButtonProps, HInput } from '@hypha/ui'
import getConversationProfile from 'app/get/getConversationProfile'
import { ConversationType } from 'app/services/Streamr_API'
import React from 'react'
import { Platform } from 'react-native'
import HomeNavButton from './HomeNavButton'
import useSelectedConversation from 'app/components/hooks/useSelectedConversation'

type NavBarProps = {
    test: () => void,
    toggleDrawer: () => void,
}

export const NavBar = ({test, toggleDrawer} : NavBarProps) => {
    const selectedConversation = useSelectedConversation();

    const topBarChildren: YStackProps & HButtonProps = {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        flex: 1,
    }

    const topBarChildrenWeb: HButtonProps = {
        display: Platform.OS === "web" ? 'flex' : 'none',
        $sm: {
            display: 'none',
        }
    }

    const topBarChildrenNative: HButtonProps = {
        display: Platform.OS !== "web" ? 'flex' : 'none',
    }
    
    return (
        <YStack
            flexDirection='row'
            justifyContent='flex-start'
            alignItems='center'
            height='$navHeight'
            backgroundColor='$uiBGColor'
        >
            <HButton
                hypha="standard"
                onPress={() => toggleDrawer()}
                {...{ ...topBarChildren, ...topBarChildrenNative }}
                flex={.2}
            >
                <HButton.Text
                    color='$uiColor'
                    userSelect='none'
                >
                    ☰
                </HButton.Text>
            </HButton>
            <HomeNavButton
                type='LogoOnly'
                {...{ ...topBarChildren, ...topBarChildrenWeb }}
            />
            <YStack
                {...topBarChildren}
            >
                <SizableText
                    color='$uiColor'
                >
                    {
                        selectedConversation.streamId !== '' ?
                            selectedConversation.type === ConversationType.Hypha ?
                                getConversationProfile(selectedConversation)?.name
                                :
                                selectedConversation.streamId
                            :
                            'Select Conversation'
                    }
                </SizableText>
            </YStack>
            <YStack
                {...topBarChildren}
            >
                <HInput
                    hypha='unset'
                    placeholder="Search &#x1F50D;"
                    placeholderTextColor='$uiColor'
                    // onChange={(e) => setSearchKey(e.target.value)}
                    backgroundColor='transparent'
                    color='$uiColor'
                    width='100%'
                    height='100%'
                    textAlign='center'
                    borderColor={'transparent'}
                />
            </YStack>
            <HButton
                hypha="standard"
                onPress={() => test()}
                {...{ ...topBarChildren, ...topBarChildrenWeb }}
            >
                <SizableText
                    color='$uiColor'
                    userSelect='none'
                >
                    Notifications
                </SizableText>
            </HButton>
        </YStack>
    )
}