import React, { useContext, useState } from "react";
// import styles from '../styles/sendmessage.module.css'
import { MessageContext } from "./MessageContext";
import { PriceFeeds } from "./PriceFeeds";
import { EmojiMenu } from "./EmojiMenu";
import { StateContext } from "app/context/state";
import { HButton, getVariableValue, Input, SizableText, useTheme, YStack, YStackProps, HInput } from "@hypha/ui";

type SendMessageProps = {
  disable: boolean
  typing: (typing: boolean) => void
  sendMessage: (messageText: string) => void
}

export const SendMessage = ( {disable, typing, sendMessage} : SendMessageProps ) => {
  // const { ipfs } = useContext(StateContext);
  const [inputValue, setInputValue] = useState("");
  const [showMessageContext, setShowMessageContext] = useState<string>('');
  const [showEmojiMenu, setShowEmojiMenu] = useState<boolean>(false);
  const [showPriceFeeds, setShowPriceFeeds] = useState<boolean>(false);

  const keyHandler = (keyEvent: string) => {
    if (keyEvent === "Enter" && inputValue.trim() !== "") {
      sendMessage(inputValue);
      setInputValue("");
      typing(false);
    }
  };

  const buttonHandler = () => {
    if (inputValue.trim() !== ""){
      sendMessage(inputValue);
      setInputValue("");
      typing(false);
    }
  };

  const inputChangeHandler = (value: string) => {
    setInputValue(value);
    typing(value !== '' && !value.startsWith('@') && !value.startsWith(':') && !value.startsWith('/'));
  }

  // async function onChange(currentTarget: EventTarget & HTMLInputElement) {
  //   console.log(currentTarget.files[0].name);
  //   try {
  //     //Upload file, wait until completed, then send message
  //     ipfs.add(
  //       {
  //         path: currentTarget.files[0].name,
  //         content: currentTarget.files[0]
  //       }, 
  //       {
  //         wrapWithDirectory: true
  //       }
  //     ).then((res) => {
  //       sendMessage(`https://ipfs.io/ipfs/${res.cid.toString()}/${currentTarget.files[0].name}`);
  //       console.log(res);
  //     });
  //   } catch (error) {
  //     console.log('Error uploading file: ', error)
  //   }
  // }

  const childrenContainer: YStackProps = {
    // flex: .25,
    paddingHorizontal: '2%',
  }

  return (
    // id={styles.sendMessage}
    <YStack 
      flexDirection='row'
      justifyContent='space-between'
      alignItems='center'
      backgroundColor='$uiBGColor'
      minHeight={50}
      margin='1.5%'
      paddingHorizontal='1%'
      borderRadius={20}
      overflow='hidden'
    >
      <YStack 
        {...childrenContainer}
      >
        <HButton
          hypha="standard"
          circular
          backgroundColor='$olive'
          hoverStyle={{
            backgroundColor: '$pear',
          }}
        >
          <SizableText
            color='$pear'
            fontSize='$5'
            padding='25%'
            width='100%'
            textAlign="center"
            userSelect='none'
            scale={1.5}
            hoverStyle={{
              color: '$olive',
            }}
          >
            +
          </SizableText>
        </HButton>
        {/* <label id={styles.addFileLabel} htmlFor={styles.addFile}>+</label>
        <input id={styles.addFile} type="file" onChange={({currentTarget}) => onChange(currentTarget)} disabled={disable}></input> */}
      </YStack>
      <HInput
        hypha="unset"
        // id={styles.messageText}
        // name="message"
        // type="text"
        textContentType="none"
        placeholder="message"
        placeholderTextColor='$inputBGColor'
        autoComplete="off"
        value={inputValue}
        onChange={({nativeEvent}) => inputChangeHandler(nativeEvent.text)}
        // onInput={({currentTarget}) => setShowMessageContext(currentTarget.value)}
        onKeyPress={({nativeEvent}) => keyHandler(nativeEvent.key)}
        // disabled={disable}
        selectTextOnFocus={!disable}
        editable={!disable}
        color='$inputColor'
        flex={1}
        backgroundColor='transparent'
      />
      {/* Message Context */}
      {/* className={styles.plugins} */}
      <YStack 
        {...childrenContainer}
      >
        {/* <MessageContext
          inputValue={showMessageContext}
          onClick={(value: string) => setInputValue(inputValue + value)}
          cancel={() => setShowMessageContext('')}
        /> */}
        <HButton
          hypha="unset"
          // id={styles.priceFeed}
          onPress={!disable ? () => setShowPriceFeeds(!showPriceFeeds) : () => {}}
          opacity={1}
          paddingHorizontal='1%'
        >
          <SizableText 
            // fontSize='$6'
            fontWeight='900'
            letterSpacing={1}
            transform={[{ rotate: '30deg' }, { scale: 1.5}]}
            userSelect='none'
            hoverStyle={{
              fontWeight: '900',
              color: '$redstone1',
              // textShadow: 1px 0 var(--redstone2),
              letterSpacing: 1,
            }}
          >
            &#x2B21;
          </SizableText>
        </HButton>
      </YStack>
      {/* Price Feeds */}
      {/* className={styles.plugins} */}
      <YStack 
        {...childrenContainer}
      >
        {/* <PriceFeeds
          show={showPriceFeeds}
          onClick={(value: string) => {
            setInputValue(inputValue + value);
          }}
          cancel={() => setShowPriceFeeds(!showPriceFeeds)}
        /> */}
        <HButton
          hypha="unset"
          // id={styles.pickEmoji}
          onPress={!disable ? () => setShowEmojiMenu(!showEmojiMenu) : () => {}}
          opacity={1}
          paddingHorizontal='1%'
        >
          <SizableText 
            // filter: opacity(0.5) drop-shadow(0 0 0 var(--inputColor))
            transform={[{scale: 1.2}]}
            fontSize='$4'
            opacity={.5}
            userSelect='none'
            hoverStyle={{
              // filter: 'none',
              scale: 1.3,
              opacity: 1,
            }}
          >
            &#x1F60A;
          </SizableText>
        </HButton>
      </YStack>
      {/* Emoji Menu */}
      {/* className={styles.plugins} */}
      <YStack 
        {...childrenContainer}
      >
        {/* <EmojiMenu
          show={showEmojiMenu}
          onClick={(value: string) => {
            setInputValue(inputValue + value);
          }}
          cancel={() => setShowEmojiMenu(!showEmojiMenu)}
        /> */}
        <HButton
          hypha="unset"
          // id={styles.messageSubmit} 
          onPress={!disable ? () => buttonHandler() : () => {}}
          opacity={1}
          paddingHorizontal='1%'
        >
          <SizableText 
            fontSize='$4'
            userSelect='none'
            hoverStyle={{
              color: '$inputHoverBGColor',
            }}
          >
            Send
          </SizableText>
        </HButton>
      </YStack>
    </YStack>
  );
};