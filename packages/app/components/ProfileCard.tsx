import React from 'react'
import getProfilePicture from '../get/getProfilePicture';
import ProfileImage from './ProfileImage'
import { Profile } from './utils/Types'
import { Shadow } from 'react-native-shadow-2'
import { 
    Button, 
    H3, 
    H5, 
    XStack, 
    YStack, 
    YStackProps, 
    TextProps, 
    getVariableValue, 
    getTokens, 
    Circle, 
    SizableText, 
    useWindowDimensions,
    HButton
} from '@hypha/ui';
import { HTooltip, ImageBackground } from '@hypha/ui/components'

export default function ProfileCard({ profile, portrait = true } : { profile: Profile, portrait?: boolean }) {
    const { width } = useWindowDimensions();
    const imageSize = 128;
    const imageContainerSize = imageSize + 16;
    const tokens = getTokens();

    type orientation = {
        container: YStackProps,
        contentContainer: YStackProps,
        content: YStackProps,
        h3: TextProps,
        h5: TextProps
    }

    const portraitStyle: orientation = {
        container: {
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
            width: width * .2,
            aspectRatio: 2/3,
            $sm: {
                width: '100%'
            }
        },
        contentContainer: {
            justifyContent: 'center',
            alignItems: 'flex-start',
            height: '50%',
            width: '100%',
        },
        content: {
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
            width: '100%',
            top: imageContainerSize,
        },
        h3: {

        },
        h5: {
            margin: 0,
        }
    }

    const landscapeStyle: orientation = {
        container: {
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
            width: width * .3,
            aspectRatio: 3/2,
            $sm: {
                width: '100%'
            }
        },
        contentContainer: {
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            /* Half container height - half image size */
            height: '50%',
            transform: [{translateY: -imageContainerSize / 2}],
            width: '100%',
        },
        content: {
            justifyContent: 'flex-end',
            alignItems: 'flex-start',
            flexDirection: 'column',
            zIndex: 1,
            width: '100%',
            paddingLeft: '5%',
            top: imageContainerSize,
        },
        h3: {
            // fontSize: h3FontSize * .75,
            marginVertical: 0,
            $sm: {
                marginVertical: '5%',
            }
        },
        h5: {
            marginHorizontal: 0,
            marginVertical: 0,
            // fontSize: h5FontSize * .75,
            fontWeight: 'unset',
            $sm: {
                marginVertical: '5%',
                fontWeight: '300',
            }
        }
    }

    // const shadow: { shadow: StyleProp<ViewStyle>, shadowContainer: StyleProp<ViewStyle>} = {
    //     shadow: {
    //         borderRadius: 314, 
    //         overflow: 'hidden'
    //     },
    //     shadowContainer: {
    //         left: '10%',
    //     }
    // }

    const orientationStyle: orientation = portrait ? portraitStyle : landscapeStyle

    return (
        <XStack
            onPress={(e) => e.stopPropagation()}
            backgroundColor='$appBGColor2'
            borderRadius={20}
            $sm={{
                borderRadius: 0,
                height: '100%',
                width: '100%'
            }}
            {...orientationStyle.container}
        >
            <ImageBackground 
                source={{uri: getProfilePicture(profile).background}}
                backgroundColor='$cannon'
                height='50%'
                borderTopLeftRadius={20}
                borderTopRightRadius={20}
                $sm={{
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0,
                }}
            />
            <YStack 
                {...orientationStyle.contentContainer}
            >
                {/* <Shadow 
                    distance={8} 
                    startColor={getVariableValue(tokens.color.pear)} 
                    endColor={getVariableValue(tokens.color.pear)} 
                    style={{borderRadius: 314, overflow: 'hidden'}} 
                    containerStyle={{left: '10%'}}
                > */}
                <Circle size={imageContainerSize} backgroundColor='$pear' left='10%' position='relative'>
                    <ProfileImage 
                        profile={profile} 
                        sizePx={imageSize} 
                        onPress={profile?.address ? () => window.open(`/profile/${profile?.address}`) : null}
                    />
                </Circle>
                {/* </Shadow> */}
                <YStack 
                    {...orientationStyle.content}
                >
                    {/* <HTooltip content={profile?.address}> */}
                        <H3 
                            // fontSize=h3FontSize,
                            marginVertical='5%'
                            color='$appColor'
                            {...orientationStyle.h3}
                        >
                            <HButton onPress={async () => navigator.clipboard.writeText(profile?.address)} hypha="unset">
                                <SizableText>{profile?.name || profile?.address}</SizableText>
                            </HButton>
                        </H3>
                    {/* </HTooltip> */}
                    <H5 
                        // fontSize=h5FontSize,
                        fontWeight='300'
                        marginVertical='5%'
                        color='$appColor'
                        {...orientationStyle.h5}
                    >
                        {profile?.description}
                    </H5>
                </YStack>
            </YStack>
        </XStack>
    )
}