import React from 'react';
import { useState } from 'react'
import { HButtonProps, H1, useWindowDimensions, HButton, Stack, getTokens, getVariableValue, useMedia } from '@hypha/ui';
import { Logo } from '@hypha/ui/components'
import { useRouter } from 'solito/router';

type NavTypes = 
| 'Both'
| 'LogoOnly'
| 'TextOnly'

interface HomeNavButtonProps extends HButtonProps {
    type?: NavTypes
}

export default function HomeNavButton({type = 'Both', ...style} : HomeNavButtonProps) {
    const [headerWidth, setHeaderWidth] = useState(0);
    const { width } = useWindowDimensions();
    const { push } = useRouter();
    const tokens = getTokens();
    const { sm } = useMedia();

	return (
        <HButton
            hypha='unset'
            onPress={() => push('/')}
            display='flex'
            cursor='pointer'
            marginLeft={width * .02}
            alignItems='center'
            justifyContent='center'
            maxHeight='$logoSize'
            userSelect='none'
            width='auto'
            height='100%'
            {...style}
        >
            {
                type === 'Both' || type === 'LogoOnly' ?
                <Logo.Hypha01 
                    height={sm ? getVariableValue(tokens.size.$mobileLogoSize) : getVariableValue(tokens.size.$logoSize)}
                    width={sm ? getVariableValue(tokens.size.$mobileLogoSize) : getVariableValue(tokens.size.$logoSize)}
                />
                :
                null
            }
            {
                type === 'Both' || type === 'TextOnly' ?
                <Stack
                    onLayout={event => {setHeaderWidth(event.nativeEvent.layout.width)}}
                    paddingLeft={width * .01}
                    //Positions text in center of screen on mobile devices
                    left='unset'
                    $sm={{
                        paddingLeft: 0,
                        left: width * .5 - headerWidth / 2,
                        position: 'absolute',
                    }}
                >
                    <H1 
                        fontSize='$6'
                        position='relative'
                        color='$olive'
                        selectable={false}
                        cursor='pointer'
                        >
                            HYPHA
                    </H1>
                </Stack>
                :
                null
            }
        </HButton>
    );
}
