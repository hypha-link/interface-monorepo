import React, { useEffect, useState } from 'react'
import { Metadata, Profile } from './utils/Types';
import Typing from './utils/Typing';
import getProfilePicture from '../get/getProfilePicture';
import { Image, YStack, HButton, HButtonProps, getVariableValue, getTokens, ColorTokens, useTheme } from '@hypha/ui';
import { Placeholder, HTooltip } from '@hypha/ui/components'
import getGrayscaleHex from 'app/get/getGrayscaleHex';

interface ProfileImageProps extends HButtonProps {
    profile: Profile
    override?: string
    metadata?: Metadata
    sizePx?: number
    fill?: ColorTokens | (string & {})
    grayscale?: boolean
    onPress?: () => void
    children?: React.ReactNode
}

/**
 * @param profile The profile of the user
 * @param override Provides the optional ability to override profile with a image URI string
 * @param metadata The user metadata for the tooltip
 * @param sizePx The size of the profile in pixels
 * @param fill The color to fill the background & for hover/press effects
 * @param grayscale Determines if the fill will be shown in grayscale instead
 * @returns The profile image of the user specified, along with tooltips for related metadata
 */
export default function ProfileImage({ profile, override, metadata, sizePx = 50, fill, grayscale = false, onPress, children, ...style }: ProfileImageProps) {
    const [fillToken, setFillToken] = useState<any>(undefined);
    const image: string = override ? override : getProfilePicture(profile).image;
    const tokens = getTokens();
    const theme = useTheme();
    const fallbackFill = getVariableValue(theme.appColor);

    // Assign fillValue if fill is a color token
    useEffect(() => {
        if (fill) {
            if (fill in theme) {
                setFillToken(theme[fill])
            }
            else if (fill in tokens.color) {
                setFillToken(tokens.color[fill])
            }
            else if (fill.includes('#')) {
                setFillToken(fill)
            }
            else {
                setFillToken(undefined)
            }
        }
    }, [fill])

    const ImageComponent = () => {
        return (
            image ?
                <Image
                    source={{ uri: image }}
                    accessible
                    accessibilityLabel={'Profile Image'}
                    borderRadius={314}
                    maxHeight={sizePx}
                    maxWidth={sizePx}
                    height={sizePx}
                    width={sizePx}
                    zIndex={1}
                    backgroundColor={
                        grayscale && fillToken ?
                            getGrayscaleHex(getVariableValue(fillToken))
                            :
                            getVariableValue(fillToken) || fallbackFill
                    }
                />
                
                :

                <YStack
                    borderRadius={314}
                    overflow='hidden'
                    maxHeight={sizePx}
                    maxWidth={sizePx}
                    zIndex={1}
                >
                    <Placeholder.UserPlaceholder
                        height={sizePx}
                        width={sizePx}
                        fill={
                            grayscale && fillToken ?
                                getGrayscaleHex(getVariableValue(fillToken))
                                :
                                getVariableValue(fillToken) || fallbackFill
                        }
                    />
                </YStack>
        )
    }

    const MetadataComponent = () => {
        return (
            // Show metadata if valid
            metadata
                ?
                <YStack
                    position='absolute'
                    bottom={0}
                    right={0}
                    height='$3'
                    width='$3'
                >
                    {/* <HTooltip content={metadata.online ? 'Online' : 'Offline'} direction='top' delay={100}>
                        <Typing metadata={metadata}/>
                    </HTooltip> */}
                </YStack>
                :
                null
        )
    }

    return (
        // Create a button if onPress was set
        onPress ?

            <HButton
                hypha="unset"
                position='relative'
                maxHeight={sizePx}
                maxWidth={sizePx}
                height={sizePx}
                width={sizePx}
                cursor={onPress ? 'pointer' : null}
                hoverStyle={
                    image ?
                        { backgroundColor: getGrayscaleHex(getVariableValue(fillToken)), borderRadius: '$1' }
                        :
                        { backgroundColor: 'transparent' }
                }
                pressStyle={
                    image ?
                        { backgroundColor: getGrayscaleHex(getVariableValue(fillToken)), borderRadius: '$1' }
                        :
                        { backgroundColor: 'transparent' }
                }
                onPress={(e) => {
                    //Confirm that onPress is valid
                    onPress && onPress();
                    e.stopPropagation();
                }}
                {...style}
            >
                <ImageComponent />
                {children}
                <MetadataComponent />
            </HButton>

            :

            <>
                <ImageComponent />
                {children}
                <MetadataComponent />
            </>
    )
}
