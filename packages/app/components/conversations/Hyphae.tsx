import React from 'react'
import { Conversations } from '../utils/Types';
import ContextMenu from '../ContextMenu';
import ConversationComponent from './ConversationComponent';

type HyphaeProps = {
  conversation: Conversations
  inviteConversation?: (conversation: Conversations) => void
  selectConversation: (conversation: Conversations) => void
  deleteConversation?: (conversation: Conversations) => void
}

export default function Hyphae({conversation, inviteConversation, selectConversation, deleteConversation}: HyphaeProps) {
  return (
    <ContextMenu
      options={[
        {name: 'Select', fn: () => !conversation.selected && selectConversation(conversation)},
        {name: 'View', fn: () => console.log(conversation || "No profile exists.")},
        {name: 'Invite', fn: () => inviteConversation(conversation)},
        {name: 'Leave_Hyphae', fn: () => deleteConversation(conversation)}
      ]}
      conversation={conversation}
      onPress={() => {
        //If not selected, allow user to select conversation
        !conversation.selected && selectConversation(conversation)
      }}
    >
      <ConversationComponent conversation={conversation}/>
    </ContextMenu>
  )
}