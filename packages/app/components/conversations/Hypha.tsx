import { useSendTransaction } from '@usedapp/core';
import React from 'react'
import { Conversations } from '../utils/Types';
import ContextMenu from '../ContextMenu';
import { utils } from 'ethers';
import getConversationProfile from '../../get/getConversationProfile';
import ConversationComponent from './ConversationComponent';

type HyphaProps = {
  conversation: Conversations
  inviteConversation?: (conversation: Conversations) => void
  selectConversation: (conversation: Conversations) => void
  deleteConversation?: (conversation: Conversations) => void
}

export default function Hypha({conversation, inviteConversation, selectConversation, deleteConversation}: HyphaProps) {
  const { sendTransaction } = useSendTransaction();
  const profile = getConversationProfile(conversation);

  return (
    <ContextMenu
      options={[
        {name: 'Select', fn: () => !conversation.selected && selectConversation(conversation)},
        {name: 'View', fn: () => console.log(conversation || "No profile exists.")},
        {name: 'Invite', fn: () => inviteConversation(conversation)},
        {name: 'Send', fn: () => {
          sendTransaction({
            to: profile.address,
            value: utils.parseEther(".1"),
          });
        }},
        {name: 'Delete', fn: () => deleteConversation(conversation)}
      ]}
      conversation={conversation}
      onPress={() => {
        //If not selected, allow user to select conversation
        !conversation.selected && selectConversation(conversation)
      }}
    >
      <ConversationComponent conversation={conversation}/>
    </ContextMenu>
  );
}