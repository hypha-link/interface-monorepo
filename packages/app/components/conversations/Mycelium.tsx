import React from 'react'
import { Conversations } from '../utils/Types';
import ContextMenu from '../ContextMenu';
import ConversationComponent from './ConversationComponent';

type MyceliumProps = {
  conversation: Conversations
  inviteConversation?: (conversation: Conversations) => void
  selectConversation: (conversation: Conversations) => void
  deleteConversation?: (conversation: Conversations) => void
}

export default function Mycelium({conversation, inviteConversation, selectConversation, deleteConversation}: MyceliumProps) {
  return (
    <ContextMenu
      options={[
        {name: 'Select', fn: () => !conversation.selected && selectConversation(conversation)},
        {name: 'View', fn: () => console.log(conversation || "No profile exists.")},
        {name: 'Invite', fn: () => inviteConversation(conversation)},
        {name: 'Leave_Mycelium', fn: () => deleteConversation(conversation)}
      ]}
      conversation={conversation}
      onPress={() => {
        //If not selected, allow user to select conversation
        !conversation.selected && selectConversation(conversation)
      }}
    >
      <ConversationComponent conversation={conversation}/>
    </ContextMenu>
  )
}