import React, { forwardRef } from 'react'
import { Paragraph, Spacer, TamaguiElement, XStack } from '@hypha/ui'
import ProfileImage from '../ProfileImage'
import getConversationProfile from 'app/get/getConversationProfile'
import getConversationName from 'app/get/getConversationName'
import { Conversations } from '../utils/Types'
// import { ConversationType } from 'app/services/Streamr_API'
// import { HTooltip } from '@hypha/ui/components'
// import { OcclusionUpdateContext } from '../utils/Occlusion';
// import ProfileCard from '../ProfileCard';

type alignTypes = 
    | "center" 
    | "flex-start" 
    | "flex-end" 
    | "space-between" 
    | "space-around" 
    | "space-evenly"

const ConversationComponent = forwardRef(function ConversationComponent({ conversation, align = 'flex-start' }: { conversation: Conversations, align?: alignTypes }, ref: React.Ref<TamaguiElement> ) {
    const profile = getConversationProfile(conversation);
    // const setOcclusionState = useContext(OcclusionUpdateContext);
    return (
        <XStack
            backgroundColor='transparent'
            width='100%'
            ai='center'
            jc={align}
            flex={1}
            paddingVertical='$1'
            paddingHorizontal='$3'
            ref={ref}
        >
            {/* <HTooltip key={Math.random()} content={conversation.type === ConversationType.Hypha ? profile?.address : conversation.streamId}> */}
                <ProfileImage
                    profile={profile}
                    metadata={conversation.metadata}
                    disabled
                    // onPress={() => setOcclusionState(<ProfileCard profile={profile}/>)}
                />
                <Spacer size='$2' />
                <Paragraph>
                    {getConversationName(conversation)}
                </Paragraph>
            {/* </HTooltip> */}
        </XStack>
    )
})

export default ConversationComponent;