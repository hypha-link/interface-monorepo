import React from 'react'
import { Conversations } from './utils/Types'
import { ConversationType } from '../services/Streamr_API'
import Hypha from './conversations/Hypha'
import Hyphae from './conversations/Hyphae'
import Mycelium from './conversations/Mycelium'
import { getVariableValue, useTheme, YStack } from '@hypha/ui'

type ConversationProps = {
    conversation: Conversations
    inviteConversation?: (conversation: Conversations) => void
    selectConversation: (conversation: Conversations) => void
    deleteConversation?: (conversation: Conversations) => void
}

export const Conversation = ({conversation, inviteConversation, selectConversation, deleteConversation}: ConversationProps) => {
    const theme = useTheme();

    return (
        <YStack 
            width='100%'
            backgroundColor={conversation.selected ? `${getVariableValue(theme.uiHoverColor)}26` : '$appBGColor'}
            hoverStyle={{
                backgroundColor: conversation.selected ? 
                `${getVariableValue(theme.uiHoverColor)}26` 
                : 
                `${getVariableValue(theme.uiHoverColor)}0d`
            }}
        >
            {/* Conversation type selector */}
            {
                conversation.type === ConversationType.Hypha ? 
                <Hypha 
                conversation={conversation} 
                inviteConversation={inviteConversation} 
                selectConversation={selectConversation} 
                deleteConversation={deleteConversation}
                />
                : conversation.type === ConversationType.Hyphae ? 
                <Hyphae 
                conversation={conversation} 
                inviteConversation={inviteConversation} 
                selectConversation={selectConversation} 
                deleteConversation={deleteConversation}
                />
                : conversation.type === ConversationType.Mycelium ? 
                <Mycelium 
                conversation={conversation} 
                inviteConversation={inviteConversation} 
                selectConversation={selectConversation} 
                deleteConversation={deleteConversation}
                />
                : 
                <></>
            }
        </YStack>
    )
}