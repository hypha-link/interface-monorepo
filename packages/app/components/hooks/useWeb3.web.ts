import React, { useContext, useEffect, useState } from 'react';
import { DispatchContext, StateContext } from 'app/context/state';
import { Actions } from 'app/provider/state';
import { ethers } from 'ethers';

export default function useWeb3() {
	const [connected, setConnected] = useState(false);
	const [items, setItems] = useState<{provider: ethers.providers.Web3Provider, accounts: string[]}>(undefined);
	const { web3Provider, ownProfile } = useContext(StateContext);
	const dispatch = useContext(DispatchContext);

	// Set global state
	useEffect(() => {
		if(items){
			dispatch({ type: Actions.SET_WEB3_PROVIDER, payload: items.provider });
			dispatch({ type: Actions.SET_ACCOUNT, payload: items.accounts[0] });
			setConnected(true);
		}
	}, [items])

	/**
	 * Connect to Web3 Provider
	 */
	const connect = async () => {
		// Create Provider for Browser
		const createWebProvider = async () => {
			try {
				// Create new Web3Provider using ethers
				const provider = new ethers.providers.Web3Provider(window.ethereum);
				// Activate
				const accounts: string[] = await provider.send("eth_requestAccounts", []);
				return {provider: provider, accounts: accounts}
			}
			catch(err){
				console.info(err);
				return;
			}
		}
		
		try{
			// Select platform & create provider
			setItems(await createWebProvider())
		}
		catch(err){
			console.error('Failed to set Web3Provider: ', err);
		}
	}

	/**
	 * Disconnect from Web3 Provider
	 */
	const disconnect = async () => {
		dispatch({ type: Actions.SET_WEB3_PROVIDER, payload: undefined });
		dispatch({ type: Actions.SET_ACCOUNT, payload: undefined });
		setConnected(false);
	}

	return {
		connect: connect,
		disconnect: disconnect,
		connected: connected,
		provider: web3Provider,
		address: ownProfile?.address,
	} as const;
}