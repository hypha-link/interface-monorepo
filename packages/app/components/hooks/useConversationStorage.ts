import { useContext, useEffect, useState } from 'react';
import { StateContext } from 'app/context/state';
import { Conversations } from '../utils/Types';
import { TileDocument } from "@ceramicnetwork/stream-tile"
import AsyncStorage from '@react-native-async-storage/async-storage';

export const localStreamKey = "conversations";

export default function useConversationStorage() {
    const { selfId, ownProfile, conversations } = useContext(StateContext);
    const [ceramicConversations, setCeramicConversations] = useState<Conversations[]>([]);
    const [ceramicStream, setCeramicStream] = useState<TileDocument>(undefined);

    useEffect(() => {
        const conversationsStore = async () => {
            const stream = await selfId.client.tileLoader.load(await AsyncStorage.getItem(`${ownProfile?.address}-${localStreamKey}`));
            setCeramicConversations(stream.content.conversations);
            setCeramicStream(stream);
        }
        //Update when ceramicConversations & conversations are not equal (or load ceramicConversations for first time)
        const shouldUpdateConversations = conversations?.every((conversation, i) => {
            conversation.profile === ceramicConversations[i]?.profile &&
            conversation.selected === ceramicConversations[i]?.selected &&
            conversation.streamId === ceramicConversations[i]?.streamId &&
            conversation.type === ceramicConversations[i]?.type;
        })
        AsyncStorage.getItem(`${ownProfile?.address}-${localStreamKey}`).then(async (value) => {
            if(value !== null && selfId && shouldUpdateConversations) await conversationsStore();
        })
    }, [selfId, conversations])

    return { ceramicConversations, ceramicStream };
}