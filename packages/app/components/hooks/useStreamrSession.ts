import { Wallet } from 'ethers';
import { useContext, useEffect } from 'react'
import { Actions } from 'app/provider/state/AppContextTypes';
import { DispatchContext, StateContext } from 'app/context/state'
import useEnsureCorrectNetwork from '../hooks/useEnsureCorrectNetwork';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function SessionHandler() {
    const { ownProfile, web3Provider } = useContext(StateContext);
    const dispatch = useContext(DispatchContext);
    const ensureCorrectNetwork = useEnsureCorrectNetwork();
    const sessionStorage = 'encryptedPrivateKey';

    useEffect(() => {
        if(!web3Provider || !ownProfile?.address || !ensureCorrectNetwork){
            return
        }

        async function retrieve(){
            const session = await AsyncStorage.getItem(sessionStorage)
            const encryptedPrivateKey = JSON.parse(session);
            await ensureCorrectNetwork();

            //Create new wallet
            if(!encryptedPrivateKey){
                const wallet = Wallet.createRandom()
                const encrypted = await wallet.encrypt(ownProfile.address)
                await AsyncStorage.setItem(sessionStorage, JSON.stringify(encrypted));
            }
            else{
                const wallet = await Wallet.fromEncryptedJson(encryptedPrivateKey, ownProfile.address);
                dispatch({ type: Actions.SET_STREAMR, payload: wallet });
            }
        }

        retrieve();
    }, [web3Provider, ownProfile?.address,])
}
