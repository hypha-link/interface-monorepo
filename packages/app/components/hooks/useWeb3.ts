import React, { useContext, useEffect, useState } from 'react';
import { useWalletConnectModal } from '@walletconnect/modal-react-native'
import { DispatchContext, StateContext } from 'app/context/state';
import { Actions } from 'app/provider/state';
import { ethers } from 'ethers';
import { Platform } from 'react-native';

export default function useWeb3() {
	const [connected, setConnected] = useState(false);
	const [items, setItems] = useState<{provider: ethers.providers.Web3Provider, accounts: string[]}>(undefined);
	const { provider: wcProvider, address, open, isConnected } = useWalletConnectModal();
	const { web3Provider, ownProfile } = useContext(StateContext);
	const dispatch = useContext(DispatchContext);

	// Set global state
	useEffect(() => {
		if(items){
			dispatch({ type: Actions.SET_WEB3_PROVIDER, payload: items.provider });
			dispatch({ type: Actions.SET_ACCOUNT, payload: items.accounts[0] });
			setConnected(true);
		}
	}, [items])

	// Catch rn wallet connect connection changes & update state
	useEffect(() => {
		if(isConnected && wcProvider){
			const provider = new ethers.providers.Web3Provider(wcProvider);
			setItems({provider: provider, accounts: [address]})
		}
	}, [isConnected, wcProvider])

	/**
	 * Connect to Web3 Provider
	 */
	const connect = async () => {
		// Create Provider for Browser
		const createWebProvider = async () => {
			try {
				// Create new Web3Provider using ethers
				const provider = new ethers.providers.Web3Provider(window.ethereum);
				// Activate
				const accounts: string[] = await provider.send("eth_requestAccounts", []);
				return {provider: provider, accounts: accounts}
			}
			catch(err){
				console.info(err);
				return;
			}
		}
		// Create Provider for React Native
		const createRNProvider = async () => {
			try {
				// Open WalletConnect modal
				await open({route: 'ConnectWallet'})
			}
			catch(err){
				console.info(err);
			}
		}
		
		try{
			// Select platform & create provider
			Platform.OS !== "web" ? await createRNProvider() : setItems(await createWebProvider())
		}
		catch(err){
			console.error('Failed to set Web3Provider: ', err);
		}
	}

	/**
	 * Disconnect from Web3 Provider
	 */
	const disconnect = async () => {
		Platform.OS !== "web" && await wcProvider.disconnect();
		dispatch({ type: Actions.SET_WEB3_PROVIDER, payload: undefined });
		dispatch({ type: Actions.SET_ACCOUNT, payload: undefined });
		setConnected(false);
	}

	return {
		connect: connect,
		disconnect: disconnect,
		connected: connected,
		provider: web3Provider,
		address: ownProfile?.address,
	} as const;
}