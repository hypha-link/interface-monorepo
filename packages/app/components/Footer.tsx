import React from 'react'
import { Linking } from 'react-native'
import { XStack, Anchor, useTheme, getVariableValue, AnchorProps, HButton, SizableText } from '@hypha/ui'
import { FA, SvgProps } from '@hypha/ui/components'

export default function Footer() {
  const theme = useTheme();

  const socialText: AnchorProps = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    color: '$inputColor',
    marginHorizontal: '5%', 
    height: '100%',
    hoverStyle:{
      scale: 1.1
    },
  }

  const socialSvg: SvgProps = {
    height: 24, 
    width: 24, 
    fill: getVariableValue(theme.appColor),
  }

  return (
    <XStack 
      justifyContent='space-around'
      alignItems='center'
      height='$navHeight'
      backgroundColor='$appBGColor'
      overflow='hidden'
    >
        <XStack 
          justifyContent='center'
          alignItems='center'
          flex={1}
          height='100%'
        >
            <Anchor 
              href='https://gitlab.com/hypha-link'
              target='_blank'
              rel='noreferrer'
              {...socialText}
            >
              <FA.Gitlab 
                {...socialSvg}
              />
            </Anchor>
            <Anchor 
              href='https://twitter.com/hyphalink'
              target='_blank'
              rel='noreferrer'
              {...socialText}
            >
              <FA.Twitter 
                {...socialSvg}
              />
            </Anchor>
            <Anchor 
              href='https://www.youtube.com/channel/UC2lOBy3z83CXh0Ww66qdPOw'
              target='_blank'
              rel='noreferrer'
              {...socialText}
            >
              <FA.Youtube 
                {...socialSvg}
              />
            </Anchor>
            {/* <TextLink href='https://medium.com/@hypha'>Medium</TextLink> */}
            {/* <TextLink href='#'>Hypha Discussion Server (Future)</TextLink> */}
        </XStack>
        {/* <View> */}
            {/* <TextLink href='#Privacy'>Privacy Policy</TextLink> */}
            {/* <TextLink href='#Terms'>Terms of Use</TextLink> */}
        {/* </View> */}
        <XStack 
          display='flex'
          flexDirection='row'
          justifyContent='center'
          alignItems='center'
          flex={1}
          height='100%'
          $sm={{
            display: 'none',
          }}
        >
            {/* <TextLink href='#FAQ'>FAQ</TextLink> */}
            <HButton
              hypha='unset'
              onPress={async () => await Linking.openURL('mailto:connect@hypha.link')}
              marginHorizontal='5%'
              height='60%'
              padding='1%'
              hoverStyle={{
                scale: 1.1
              }}
            >
              <SizableText
                fontSize='$3'
                color='$inputBGColor'
              >
                Connect
              </SizableText>
            </HButton>
        </XStack>
    </XStack>
  )
}
