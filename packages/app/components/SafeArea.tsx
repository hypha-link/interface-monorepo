import { YStack, YStackProps } from '@hypha/ui'
import React from 'react'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

interface StackProps extends YStackProps {
    children: React.ReactNode
}

type SafeAreaProps = React.FC<StackProps>

const SafeArea: SafeAreaProps = props => {
    const insets = useSafeAreaInsets();
    return (
        <YStack
            display='flex'
            paddingTop={insets.top} 
            paddingBottom={insets.bottom} 
            paddingLeft={insets.left} 
            paddingRight={insets.right} 
            flex={1}
            {...props}
        >
            {props.children}
        </YStack>
    )
}

export default SafeArea