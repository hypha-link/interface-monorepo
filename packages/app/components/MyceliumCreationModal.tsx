import { Fieldset, HButton, HButtonProps, HInput, Label, Sheet, SizableText, XStack, YStack } from '@hypha/ui';
import { SvgBackground } from '@hypha/ui/components';
import React, { useState } from 'react'

interface MyceliumCreationModalType extends HButtonProps {
    create: (inputValue: string) => void
    cancel: () => void
}

export function MyceliumCreationModal({create, cancel, ...style}: MyceliumCreationModalType){
    const [inputValue, setInputValue] = useState("");
    const [placeholderText, setPlaceholderText] = useState('');
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [position, setPosition] = useState<number>(0)

    const keyHandler = (e) => {
        if (e.key === "Enter") {
            handleSubmission();
        }
    };

    const handleSubmission = () => {
        if(inputValue.length < 3){
            setPlaceholderText('Please make sure the name is 3 characters or longer');
            setInputValue('');
        }
        else{
            create(inputValue);
            setInputValue('');
            cancel();
        }
    }

    return (
        <>
            <HButton
                {...style}
                onPress={() => setOpenModal(!openModal)}
                // disabled={!selfId}
            >
                <HButton.Text
                    color='$appColor'
                    userSelect='none'
                >
                    Show Mycelium
                </HButton.Text>
			</HButton>
            <Sheet
                modal
                open={openModal}
                onOpenChange={setOpenModal}
                snapPoints={[40]}
                position={position}
                onPositionChange={setPosition}
                dismissOnSnapToBottom
            >
                <Sheet.Overlay backgroundColor="#00000090"/>
                <Sheet.Frame backgroundColor='$cannon'>
                    <SvgBackground
                        flexBasis='100%'
                        type='HyphaPattern'
                        backgroundColor='$cannon'
                    >
                        <YStack
                            flex={1}
                            justifyContent='space-between'
                            paddingVertical='$4'
                        >
                            <SizableText
                                color='$uiColor'
                                fontSize='$5'
                                fontWeight='$5'
                                textAlign='center'
                            >
                                Create Mycelium
                            </SizableText>
                            <Fieldset ai='center' borderColor='transparent'>
                                <Label
                                    htmlFor='name'
                                    color='$inputColor'
                                    fontSize='$4'
                                    fontWeight='$4'
                                >
                                    Name
                                </Label>
                                <HInput
                                    id='name'
                                    placeholder={placeholderText}
                                    placeholderTextColor={'$inputColor'}
                                    value={inputValue}
                                    onChange={(e) => setInputValue(e.nativeEvent.text)}
                                    // onKeyPress
                                    textAlign='center'
                                    // border: var(--inputColor) 1px solid;
                                    color='$inputColor'
                                    backgroundColor='transparent'
                                    width='75%'
                                    borderWidth={1}
                                    borderColor='$inputColor'
                                    borderStyle='solid'
                                    flexWrap='wrap'
                                    fontSize='$3'
                                    fontWeight='$6'
                                    focusStyle={{
                                        borderWidth: 1,
                                        borderColor: '$inputColor',
                                        borderStyle: 'solid'
                                    }}
                                />
                            </Fieldset>
                            <XStack justifyContent='space-around'>
                                <HButton 
                                    hypha='standard'
                                    onPress={() => handleSubmission()}
                                    flexDirection='row'
                                    flex={1}
                                    jc="center"
                                    ai="center"
                                    backgroundColor='transparent'
                                >
                                    <HButton.Text
                                        color='$inputColor'
                                        fontSize='$5'
                                        fontWeight='$5'
                                    >
                                        Create
                                    </HButton.Text>
                                </HButton>
                                <HButton 
                                    hypha='standard'
                                    onPress={() => {
                                        setOpenModal(false)
                                        cancel();
                                        setInputValue('');
                                    }}
                                    flex={1}
                                    jc="center"
                                    ai="center"
                                    backgroundColor='transparent'
                                >
                                    <SizableText
                                        color='$inputColor'
                                        fontSize='$5'
                                        fontWeight='$5'
                                    >
                                        Close
                                    </SizableText>
                                </HButton>
                            </XStack>
                        </YStack>
                    </SvgBackground>
                </Sheet.Frame>
            </Sheet>
        </>
    )
}
