import React, { useContext, useState } from 'react';
import { useRouter } from 'solito/router'
import { ConnectButton } from './ConnectButton';
import { Conversation } from './Conversation';
import { Conversations } from './utils/Types';
import { InviteModal } from './InviteModal';
import { ConversationModal } from './ConversationModal';
import { StateContext } from 'app/context/state';
import { useConversations } from 'app/services/conversations';
import useWeb3 from './hooks/useWeb3';
import { getTokens, getVariableValue, SizableText, useTheme, useWindowDimensions, XStack, YStack, HButton } from '@hypha/ui';
import { FA, HTooltip } from '@hypha/ui/components'
import SafeArea from 'app/components/SafeArea';

export default function Sidebar() {
	const { height } = useWindowDimensions();
    const { push } = useRouter()
    const { conversations } = useContext(StateContext);
    const Orchestrator = useConversations();
    const { address } = useWeb3();
	const tokens = getTokens();
	const theme = useTheme();

	return (
		<SafeArea>
			<YStack 
				backgroundColor='transparent'
				justifyContent='space-between'
				height={height - getVariableValue(tokens.size.navHeight)}
				flex={1}
			>
				<YStack 
					justifyContent='center'
					alignItems='center'
					minHeight={height * .12}
					hoverStyle={{
						backgroundColor: `${getVariableValue(theme.uiHoverColor)}05`,
					}}
				>
					<ConversationModal/>
				</YStack>
				<YStack 
					flex={1}
					// overflowX='hidden'
					// overflowY='auto'
					// visibility='hidden'
					hoverStyle={{
						backgroundColor: `${getVariableValue(theme.uiHoverColor)}05`,
					}}
				>
					<YStack 
						flexDirection='column'
						height='100%'
						// visibility='visible'
					>
						<SizableText 
							fontWeight='500'
							fontSize='$4'
							textAlign='center'
							marginHorizontal='3%'
							marginVertical={0}
						>
							Conversations
						</SizableText>
						{
							conversations.map((_conversation) => {
								return (
									<Conversation
										key={Math.random()}
										conversation={_conversation}
										// inviteConversation={(_conversation: Conversations) => setInvitedConversation(_conversation)}
										// selectConversation={(_conversation: Conversations) => selectConversation(_conversation)}
										// deleteConversation={(_conversation: Conversations) => deleteConversation(_conversation)}
										inviteConversation={() => {}}
										selectConversation={() => Orchestrator({ type:'SELECT_CONVERSATION', payload: _conversation })}
										deleteConversation={() => Orchestrator({ type:'DELETE_CONVERSATION', payload: _conversation })}
									/>
								);
							})
						}
					</YStack>
					{/* <InviteModal 
						invitedConversation={invitedConversation}
						createHyphae={async () => await createHyphae()}
						openMyceliumModal={() => {setShowMyceliumCreationModal(true)}}
						cancel={() => setInvitedConversation(undefined)}
					/> */}
				</YStack>
				<XStack 
					flexDirection='row'
					alignItems='center'
					justifyContent='space-evenly'
					minHeight={height * .2}
					hoverStyle={{
						backgroundColor: `${getVariableValue(theme.uiHoverColor)}05`,
					}}
					// $sm={{
					// 	flexDirection: 'column-reverse'
					// }}
				>
					<HButton
						hypha="standard"
						onPress={() => push('/settings')}
						circular
					>
						<FA.Cog 
							height={25}
							width={25}
							fill={getVariableValue(tokens.color.pear)}
						/>
					</HButton>
					{/* <HTooltip content={address}> */}
						<ConnectButton/>
					{/* </HTooltip> */}
				</XStack>
			</YStack>
		</SafeArea>
	);
}