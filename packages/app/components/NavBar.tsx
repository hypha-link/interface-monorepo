import { useState } from 'react'
import { Platform } from 'react-native';
import { Link } from 'solito/link';
import { useRouter } from 'solito/router';
import { XStack, getVariableValue, getTokens, SizableText, useWindowDimensions, SizableTextProps, HButton } from '@hypha/ui';
import HomeNavButton from './HomeNavButton';

export function NavBar (){
    const [hideMobileMenu, setHideMobileMenu] = useState(true);
    const { width } = useWindowDimensions();
    const { push } = useRouter();
    const tokens = getTokens();

    const linkText: SizableTextProps = {
        fontSize: '$4', 
        marginHorizontal: 'auto',
        marginVertical: '3%',
        fontWeight: '500', 
        color: '$inputBGColor',
        fontFamily: '$alternates',
        whiteSpace: 'nowrap',
        hoverStyle:{
            scale: 1.1
        },
    }

    return (
        <XStack 
            display={Platform.OS !== 'web' ? 'none' : 'flex'}
            justifyContent='space-around'
            alignItems='center'
            height='$navHeight'
            backgroundColor='$appBGColor'
            zIndex={9}
            $sm={{
                justifyContent: 'space-between'
            }}
        >
            <HomeNavButton/>
            <XStack 
                display='flex'
                position='relative'
                top='unset'
                left='unset'
                justifyContent='space-between'
                alignItems='center'
                height='100%'
                width={null}
                backgroundColor='unset'
                zIndex={0}
                flex={1}
                $sm={{
                    display: hideMobileMenu ? 'none' : 'flex',
                    flexDirection: 'column',
                    position: 'absolute',
                    top: getVariableValue(tokens.size.navHeight),
                    left: 0,
                    height: 'unset',
                    width: '100%',
                    backgroundColor: '$appBGColor',
                    zIndex: 9,
                }}
            >
                <XStack 
                    justifyContent='space-evenly'
                    alignItems='center'
                    height='100%'
                    flex={1}
                    marginHorizontal='15%'
                    $sm={{
                        flexDirection: 'column',
                    }}
                >
                    <Link href='/welcome'>
                        <SizableText 
                            {...linkText}
                        >
                            Docs
                        </SizableText>
                    </Link>
                    <Link href='/network'>
                        <SizableText 
                            {...linkText}
                        >
                            Network Activity
                        </SizableText>
                    </Link>
                    <Link href="/roadmap">
                        <SizableText 
                            {...linkText}
                        >
                            Roadmap
                        </SizableText>
                    </Link>
                </XStack>
                {/* <Button 
                    marginRight={width * .02}
                    // height='60%'
                    // padding={0}
                    height='60%'
                    focusStyle={{
                        borderWidth: 0,
                    }}
                    $sm={{
                        marginHorizontal: 'auto',
                    }}
                    onPress={() => push('/app')}
                > */}
                <HButton 
                    hypha='unset' 
                    marginRight={width * .02} 
                    // overflow='hidden'
                    onPress={() => push('/app')}
                >
                    <SizableText
                        color='$inputBGColor'
                        backgroundColor='unset'
                        fontSize='$3'
                        fontWeight='500'
                        fontFamily='$montserrat'
                        // borderWidth={2}
                        borderLeftWidth={2}
                        borderRightWidth={2}
                        borderTopWidth={0}
                        borderBottomWidth={0}
                        borderStyle='solid'
                        borderColor='$inputBGColor'
                        padding='10%'
                        whiteSpace='nowrap'
                        textAlign='center'
                        width={120}
                        hoverStyle={{
                            color: '$inputHoverBGColor',
                            backgroundColor: '$inputHoverColor'
                        }}
                        $sm={{
                            color: '$inputHoverBGColor',
                            backgroundColor: '$inputHoverColor',
                            fontSize: '$4',
                            marginTop: '1%',
                            marginBottom: '3%',
                            padding: 0,
                        }}
                    >
                        Enter App
                    </SizableText>
                </HButton>
                {/* </Button> */}
            </XStack>
            <SizableText 
                onPress={() => setHideMobileMenu(!hideMobileMenu)}
                display='none'
                marginHorizontal='3%'
                padding='2%'
                color='$inputBGColor'
                fontSize='$6'
                cursor='default'
                userSelect='none'
                $sm={{
                    display: 'flex',
                }}
            >
                ☰
            </SizableText>
        </XStack>
    )
}