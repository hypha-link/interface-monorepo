import React, { memo } from 'react'
import { WalletConnectModal, IProviderMetadata } from '@walletconnect/modal-react-native'
import { WALLETCONNECT_PROJECT_ID } from '@env'

const projectID = WALLETCONNECT_PROJECT_ID;

const providerMetadata: IProviderMetadata = {
    name: 'Hypha',
    description: 'Connect with Hypha',
    url: 'https://hypha.link',
    icons: ['https://hypha.link/favicon.ico'],
    redirect: {
        native: 'hypha://',
    }
}

function WalletConnectProviderComponent({ children }: { children: React.ReactNode }) {
    return (
        <>
            <WalletConnectModal
                projectId={projectID}
                providerMetadata={providerMetadata}
                sessionParams={{
                    namespaces: {
                        eip155: {
                            methods: [
                                'eth_sendTransaction',
                                'eth_signTransaction',
                                'eth_sign',
                                'personal_sign',
                                'eth_signTypedData',
                            ],
                            chains: ['eip155:137'],
                            events: ['chainChanged', 'accountsChanged'],
                        },
                    },
                }
                }
            />
            {children}
        </>
    )
}

export const WalletConnectProvider = memo(WalletConnectProviderComponent)