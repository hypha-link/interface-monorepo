import { DispatchContext, StateContext } from "app/context/state";
import { useReducer } from "react";
import { AppReducer, initialState } from "app/provider/state/reducer";

export function StateProvider({ children } : { children: React.ReactNode }){
    const [state, dispatch] = useReducer(AppReducer, initialState);
    return (
        <StateContext.Provider value={state}>
            <DispatchContext.Provider value={dispatch}>
                {children}
            </DispatchContext.Provider>
        </StateContext.Provider>
    )
}

export * from 'app/provider/state/reducer'
export * from 'app/provider/state/AppContextTypes'