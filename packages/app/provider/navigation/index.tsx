import { NavigationContainer } from '@react-navigation/native'
import * as Linking from 'expo-linking'
import { useMemo } from 'react'

export function NavigationProvider({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <NavigationContainer
      linking={useMemo(
        () => ({
          prefixes: [Linking.createURL('/')],
          config: {
            initialRouteName: 'app',
            screens: {
              onboarding: 'onboarding',
              home: '',
              app: 'app',
              appNav: 'appNav',

              //Settings
              settings: 'settings',
              profile: 'profile',
              data: 'data',
              appearance: 'appearance',
              notifications: 'notifications',
              experimental: 'experimental',
            },
          },
        }),
        []
      )}
    >
      {children}
    </NavigationContainer>
  )
}
