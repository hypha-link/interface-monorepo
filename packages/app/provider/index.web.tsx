import { NavigationProvider } from 'app/provider/navigation'
import { StateProvider } from 'app/provider/state'
import { Config, DAppProvider, Polygon } from '@usedapp/core';
import { TamaguiProvider } from 'tamagui'
import { config } from '@hypha/config'
import { PortalProvider } from '@hypha/ui';

const useDappConfig: Config = {
    networks: [Polygon],
};

export function AppProvider({ children }: { children: React.ReactNode }) {
    return (
        <NavigationProvider>
            <TamaguiProvider config={config} disableInjectCSS defaultTheme='lightTheme'>
                <StateProvider>
                    <DAppProvider config={useDappConfig}>
                        <PortalProvider>
                            <>{children}</>
                        </PortalProvider>
                    </DAppProvider>
                </StateProvider>
            </TamaguiProvider>
        </NavigationProvider>
    )
}
