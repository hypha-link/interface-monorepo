import React, { useContext, useState, ReactElement } from 'react'
import { OcclusionContext, OcclusionUpdateContext } from 'app/context/occlusion';
import { Button, useWindowDimensions, YStack } from '@hypha/ui';

export function OcclusionProvider({ children } : { children: React.ReactNode }){
    const [occlusionState, setOcclusionState] = useState<ReactElement>(null)
    return(
        <OcclusionContext.Provider value={occlusionState}>
            <OcclusionUpdateContext.Provider value={setOcclusionState}>
                {children}
            </OcclusionUpdateContext.Provider>
        </OcclusionContext.Provider>
    )
}

//Reminder: Set stopPropagation() on children

export function Occlusion({ children } : { children: React.ReactNode }) {
    const setOcclusionState = useContext(OcclusionUpdateContext);
    const { height, width } = useWindowDimensions();

    return (
        <YStack 
            position='relative'
        >
            <Button
                onPress={() => setOcclusionState(null)}
                display='flex'
                justifyContent='center'
                alignItems='center'
                //@ts-ignore
                position='fixed'
                top={height * .5}
                left={width * .5}
                // backgroundColor='rgba(0, 0, 0, .8)'
                backgroundColor='#000000cc'
                zIndex={1}
                width='100%'
                height={height}
                // transform: 'translate(-50%, -50%)',
            >
                {children}
            </Button>
        </YStack>
    )
}