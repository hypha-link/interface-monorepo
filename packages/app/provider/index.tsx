import { NavigationProvider } from 'app/provider/navigation'
import { StateProvider } from 'app/provider/state'
import { WalletConnectProvider } from 'app/provider/wallet-connect-provider'
import { TamaguiProvider } from 'tamagui'
import { config } from '@hypha/config'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { PortalProvider } from '@hypha/ui'

export function AppProvider({ children }: { children: React.ReactNode }) {
  return (
    <SafeAreaProvider>
      <NavigationProvider>
        <TamaguiProvider config={config} disableInjectCSS defaultTheme='lightTheme'>
          <StateProvider>
            <WalletConnectProvider>
              {/* <PortalProvider> */}
                {children}
              {/* </PortalProvider> */}
            </WalletConnectProvider>
          </StateProvider>
        </TamaguiProvider>
      </NavigationProvider>
    </SafeAreaProvider>
  )
}
