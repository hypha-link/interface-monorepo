import { createNativeStackNavigator, NativeStackScreenProps } from '@react-navigation/native-stack'
import App from '../../pages/app'
import Settings from '../../pages/settings'
import Onboarding from 'app/pages/onboarding'
import { createDrawerNavigator, DrawerContentComponentProps, useDrawerStatus } from '@react-navigation/drawer'
import Sidebar from 'app/components/Sidebar'
import { Profile } from 'app/pages/settings/Profile'
import { ManageData } from 'app/pages/settings/ManageData'
import { Appearance } from 'app/pages/settings/Appearance'
import { Notifications } from 'app/pages/settings/Notifications'
import { Experimental } from 'app/pages/settings/Experimental'
import { StatusBar } from 'react-native';
import type { DrawerScreenProps } from '@react-navigation/drawer'
import { useEffect, useState } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { H1 } from '@hypha/ui'

type RootStackType = {
  onboarding: undefined
  app: undefined
  settings: undefined
}
const RootStack = createNativeStackNavigator<RootStackType>()
export type RootProps = NativeStackScreenProps<RootStackType>

type SettingsStackType = {
  settingsPage: undefined
  profile: undefined
  data: undefined
  appearance: undefined
  notifications: undefined
  experimental: undefined
}
const SettingsStack = createNativeStackNavigator<SettingsStackType>()
export type SettingsProps = NativeStackScreenProps<SettingsStackType>

type DrawerStackType = {
  appPage: undefined
}
const DrawerStack = createDrawerNavigator<DrawerStackType>();
export type DrawerProps = DrawerScreenProps<DrawerStackType>

const HeaderTitle = (props: { children: string, tintColor?: string }) => {
  return (
    <H1
      textAlign='center'
      fontSize='$5'
    >
      {props.children}
    </H1>
  )
}

const SettingsComponent = () => {
  return (
    <SettingsStack.Navigator screenOptions={{headerShown: true}}>
      <SettingsStack.Screen
        name='settingsPage'
        component={Settings}
        options={{headerTitle: (props) => <HeaderTitle {...props}>User Settings</HeaderTitle>, headerTitleAlign: 'center'}}
      />
      <SettingsStack.Screen
        name='profile'
        component={Profile}
        options={{headerTitle: (props) => <HeaderTitle {...props}>Profile</HeaderTitle>, headerTitleAlign: 'center'}}
      />
      <SettingsStack.Screen
        name='data'
        component={ManageData}
        options={{headerTitle: (props) => <HeaderTitle {...props}>Manage Data</HeaderTitle>, headerTitleAlign: 'center'}}
      />
      <SettingsStack.Screen
        name='appearance'
        component={Appearance}
        options={{headerTitle: (props) => <HeaderTitle {...props}>Appearance</HeaderTitle>, headerTitleAlign: 'center'}}
      />
      <SettingsStack.Screen
        name='notifications'
        component={Notifications}
        options={{headerTitle: (props) => <HeaderTitle {...props}>Notifications</HeaderTitle>, headerTitleAlign: 'center'}}
      />
      <SettingsStack.Screen
        name='experimental'
        component={Experimental}
        options={{headerTitle: (props) => <HeaderTitle {...props}>Experimental</HeaderTitle>, headerTitleAlign: 'center'}}
      />
    </SettingsStack.Navigator>
  )
}

const AppComponent = ({route, navigation}: RootProps) => {
  return (
    <DrawerStack.Navigator drawerContent={props => <CustomDrawerContent {...props}/>} screenOptions={{headerShown: false}}>
      <DrawerStack.Screen
        name='appPage' 
        component={App}
      />
    </DrawerStack.Navigator>
  )
}

//Entry Point
export function NativeNavigation() {
  const [isFirstLaunch, setIsFirstLaunch] = useState(null);
  useEffect(() => {
    AsyncStorage.getItem("onboardingViewed").then(value => {
      if (value == null) {
        setIsFirstLaunch(true);
        AsyncStorage.setItem("onboardingViewed", "true");
      } else {
        setIsFirstLaunch(false);
      }
    })
  }, []);

  return (
    <RootStack.Navigator screenOptions={{headerShown: false}}>
      {
        isFirstLaunch &&
        <RootStack.Screen
          name='onboarding'
          component={Onboarding}
        />
      }
      <RootStack.Screen
        name='app'
        component={AppComponent}
      />
      <RootStack.Screen
        name='settings'
        component={SettingsComponent}
      />
    </RootStack.Navigator>
  )
}

function CustomDrawerContent(props: DrawerContentComponentProps) {
  const isDrawerOpen = useDrawerStatus() === 'open';
  return (
    <>
      {
        isDrawerOpen ?
        <StatusBar barStyle="dark-content" backgroundColor='white' animated={true}/>
        :
        <></>
      }
      <Sidebar/>
    </>
  );
}