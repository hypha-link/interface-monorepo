// import styles from '../styles/network.module.css'
import React from 'react'
import { NavBar } from '../components/NavBar'
import Footer from '../components/Footer'
import Head from 'next/head'
import { getTokens, getVariableValue, useWindowDimensions, YStack } from '@hypha/ui'

export default function Network() {
  const { height } = useWindowDimensions();
  const tokens = getTokens();

  return (
    <>
      <Head>
        <title>Network Activity | Hypha</title>
      </Head>
      <NavBar />
      <YStack
        style={{
          height: height - (getVariableValue(tokens.size.navHeight) * 2),
          width: '100%',
          overflow: 'hidden',
        }}
      >
        <iframe
          src='https://streamr.network/network-explorer/'
          title='Network Explorer'
          style={{ height: '100%', width: '100%' }}
        />
      </YStack>
      <Footer />
    </>
  )
}