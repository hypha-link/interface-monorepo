import React, { useRef, useState } from 'react'
import { NavBar } from '../components/NavBar'
import Footer from '../components/Footer'
import Head from 'next/head'
import { GetProps, H1, H2, H3, HButton, Paragraph, SizableTextProps, useWindowDimensions, YStack, YStackProps, ScrollView, ScrollViewProps } from '@hypha/ui'
import { SvgBackground, BulletText } from '@hypha/ui/components'
import { ScrollView as RNScrollView } from 'react-native'

export default function Roadmap(){
    const { height, width } = useWindowDimensions();
    const [currentProgressHeight, setCurrentProgressHeight] = useState(0);
    const scrollRef = useRef<RNScrollView>(null)

    const roadmap: { container: YStackProps, background: YStackProps, circle: YStackProps, content: YStackProps } = {
        container: {
            flexDirection: 'column-reverse',
            justifyContent: 'center',
            position: 'relative',
        },
        background: {
            backgroundColor: '$cannon',
            position: 'absolute',
            height: '100%',
            width: 5,
            // justifySelf: 'center',
            alignSelf: 'center',
        },
        circle: {
            position: 'absolute',
            backgroundColor: '$uiBGColor',
            borderRadius: 314,
            height: 30,
            width: 30,
        },
        content: {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            marginVertical: '3%',
        }
    }

    const milestone: { container: YStackProps, header: GetProps<typeof H1>, subHeader: GetProps<typeof H3> } = {
        container: {
            backgroundColor: '$uiBGColor',
            borderColor: '$uiColor',
            borderWidth: 5,
            borderStyle: 'solid',
            borderRadius: 314,
            width: width * .4,
            height: height * .4,
            maxWidth: 250,
            maxHeight: 250,
            marginHorizontal: '5%',
        },
        header: {
            color: '$uiColor',
            fontSize: '$5',
            width: '80%',
            textAlign: 'center',
            $sm: {
                fontSize: '$2'
            }
        },
        subHeader: {
            color: '$uiColor',
            fontSize: '$3',
            width: '80%',
            textAlign: 'center',
            $sm: {
                fontSize: '$2'
            }
        },
    }

    const list: { container: YStackProps, checked: YStackProps, text: SizableTextProps } = {
        container: {
            flexDirection: 'column',
            justifyContent: 'center',
            backgroundColor: '$appBGColor',
            borderColor: '$uiBGColor',
            borderWidth: 3,
            borderStyle: 'solid',
            borderRadius: 20,
            // lineHeight: 32,
            width: width * .4,
            maxWidth: 250,
            height: 'fit-content',
            padding: '1%',
            marginHorizontal: '5%',
        },
        checked: {
            backgroundColor: '$uiColor',
            // color: 'uiBGColor',
        },
        text: {
            fontSize: '$3',
            color: '$appColor',
            paddingRight: 2,
            $sm: {
                fontSize: '$2'
            }
        },
    }

  return (
    // @ts-expect-error
    <ScrollView ref={scrollRef} height={height}>
        <Head>
            <title>Roadmap | Hypha</title>
        </Head>
        <NavBar/>
        <YStack 
            // {...title.container}
            flexDirection='column'
            justifyContent='center'
            alignItems='center'
            backgroundColor='$appBGColor2'
            width='100%'
        >
            <YStack 
                // {...title.childContainer}
                flexDirection='column'
                justifyContent='center'
                alignItems='center'
                // textAlign='center'
                width={width * .4}
                minHeight={height * .15}
                marginVertical='5%'
                $sm={{
                    width: '100%'
                }}
            >
                <H2 
                    // {...title.header}
                    color='$uiBGColor'
                    fontFamily='$alternates'
                    textAlign='center'
                    fontSize='$6'
                >
                    Roadmap
                </H2>
                <Paragraph 
                    // {...title.text}
                    color='$uiBGColor'
                    fontSize='$4'
                    fontFamily='$alternates'
                    marginTop='2%'
                    textAlign='center'
                    $sm={{
                        fontSize: '$3'
                    }}
                >
                    Follow along Hypha&apos;s development with this handy roadmap! View our current progress, or the planned direction of the project below.
                </Paragraph>
                <HButton
                    hypha='standard'
                    onClick={() => {
                        scrollRef.current.scrollTo({y: currentProgressHeight, animated: true})
                    }}
                    marginTop='$4'
                >
                    <HButton.Text color='$inputColor' fontSize='$4' padding='$2'>
                        Current Progress
                    </HButton.Text>
                </HButton>
            </YStack>
        </YStack>
        {/* id={styles.roadmap} */}
        <YStack {...roadmap.container}>
            <YStack {...roadmap.background}/>
            {/* Chime */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Chime</H1>
                        <H3 {...milestone.subHeader}>Fall 2021</H3>
                    </YStack>
                </SvgBackground>
                <YStack {...roadmap.circle}/>
                <YStack {...{...list.container, ...list.checked}}>
                    <BulletText {...list.text}>1 to 1 Ethereum Messaging</BulletText>
                    <BulletText {...list.text}>Basic Conversation Storage</BulletText>
                    <BulletText {...list.text}>Price Feeds</BulletText>
                    <BulletText {...list.text}>Emojis</BulletText>
                    <BulletText {...list.text}>IPFS Integration</BulletText>
                </YStack>
            </YStack>
            {/* Rebranding */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Rebranding</H1>
                        <H3 {...milestone.subHeader}>December 2021</H3>
                    </YStack>
                </SvgBackground>
                <YStack {...roadmap.circle}/>
                <YStack {...{...list.container, ...list.checked}}>
                    <BulletText {...list.text}>Front Page</BulletText>
                    <BulletText {...list.text}>Notifications</BulletText>
                    <BulletText {...list.text}>Basic Encryption</BulletText>
                    <BulletText {...list.text}>Preparation for Ceramic (NextJS)</BulletText>
                    <BulletText {...list.text}>Local Storage Replacement (Cermaic)</BulletText>
                    <BulletText {...list.text}>Custom Context Menu</BulletText>
                    <BulletText {...list.text}>Rebrand to Hypha</BulletText>
                </YStack>
            </YStack>
            {/* Hyphae */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Hyphae</H1>
                        <H3 {...milestone.subHeader}>January 2022</H3>
                    </YStack>
                </SvgBackground>
                <YStack {...roadmap.circle}/>
                <YStack {...{...list.container, ...list.checked}}>
                    <BulletText {...list.text}>Hyphae (Group Messaging)</BulletText>
                    <BulletText {...list.text}>Message Query</BulletText>
                    <BulletText {...list.text}>Multi-Conversation Subscriptions</BulletText>
                    <BulletText {...list.text}>Settings</BulletText>
                    <BulletText {...list.text}>User Profiles</BulletText>
                    <BulletText {...list.text}>Branding</BulletText>
                    <BulletText {...list.text}>Tooltips</BulletText>
                    <BulletText {...list.text}>Typescript</BulletText>
                </YStack>
            </YStack>
            {/* Streamr Brubeck & Polish */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Streamr Brubeck & Polish</H1>
                        <H3 {...milestone.subHeader}>February 2022</H3>
                    </YStack>
                </SvgBackground>
                <YStack {...roadmap.circle}/>
                <YStack {...{...list.container, ...list.checked}}>
                    <BulletText {...list.text}>UI / UX Redesign</BulletText>
                    {/* <BulletText {...list.text}>NFTs in messages</BulletText> */}
                    {/* <BulletText {...list.text}>ENS Support</BulletText> */}
                    {/* <BulletText {...list.text}>Pinned Ceramic Streams</BulletText> */}
                    <BulletText {...list.text}>Streamr Brubeck Update</BulletText>
                    <BulletText {...list.text}>Metadata (typing & online indicators)</BulletText>
                </YStack>
            </YStack>
            {/* Mycelium */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Mycelium</H1>
                        <H3 {...milestone.subHeader}>March 2022</H3>
                    </YStack>
                </SvgBackground>
                <YStack {...roadmap.circle}/>
                <YStack {...{...list.container, ...list.checked}}>
                    <BulletText {...list.text}>Mycelium (Guilds)</BulletText>
                    <BulletText {...list.text}>Invitation UI</BulletText>
                </YStack>
            </YStack>
            {/* UX Improvements */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>UX Improvements</H1>
                        <H3 {...milestone.subHeader}>Spring 2022</H3>
                    </YStack>
                </SvgBackground>
                <YStack {...roadmap.circle}/>
                <YStack {...{...list.container, ...list.checked}}>
                    <BulletText {...list.text}>Markdown</BulletText>
                    <BulletText {...list.text}>Network Activity</BulletText>
                    <BulletText {...list.text}>Message Context Menu</BulletText>
                    <BulletText {...list.text}>Improved IPFS Integration</BulletText>
                    <BulletText {...list.text}>Dedicated Profile Pages</BulletText>
                    <BulletText {...list.text}>Documentation</BulletText>
                </YStack>
            </YStack>
            {/* Android/IOS App */}
            <YStack
                {...roadmap.content} 
                onLayout={(event) => {
                    setCurrentProgressHeight(event.nativeEvent.layout.y + event.nativeEvent.layout.height);
                    console.log(event.nativeEvent.layout);
            }}
            >
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Android/IOS App</H1>
                    </YStack>
                </SvgBackground>
            </YStack>
            {/* Data Unions */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Data Unions</H1>
                    </YStack>
                </SvgBackground>
            </YStack>
            {/* Audio Chat */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Audio Chat</H1>
                    </YStack>
                </SvgBackground>
            </YStack>
            {/* Video Chat */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Video Chat</H1>
                    </YStack>
                </SvgBackground>
            </YStack>
            {/* Browser Extension */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Browser Extension</H1>
                    </YStack>
                </SvgBackground>
            </YStack>
            {/* Integrations API */}
            <YStack {...roadmap.content}>
                <SvgBackground type={'HyphaPattern'} {...milestone.container}>
                    <YStack flex={1} justifyContent='center' alignItems='center'>
                        <H1 {...milestone.header}>Integrations API</H1>
                    </YStack>
                </SvgBackground>
            </YStack>
        </YStack>
        <Footer/>
    </ScrollView>
  )
}