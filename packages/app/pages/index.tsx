import React, { useEffect, useState } from 'react'
import { NavBar } from '../components/NavBar'
import Footer from '../components/Footer'
import { ethers } from 'ethers'
import ProfileCard from '../components/ProfileCard'
import useProfile from '../components/hooks/useProfile'
import { Link } from 'solito/link'
import { Platform } from 'react-native'
import { 
  H1, 
  Paragraph, 
  YStack, 
  H2, 
  XStack, 
  Input, 
  ScrollView,  
  useMedia, 
  useTheme, 
  getVariableValue, 
  getTokens, 
  SizableText, 
  YStackProps, 
  SizableTextProps, 
  useWindowDimensions
} from '@hypha/ui'

import { Logo, Patterns, SvgBackground, HGrid, SvgProps } from '@hypha/ui/components'

export default function Index() {
  const [inputValue, setInputValue] = useState("");
  const profile = useProfile(inputValue);
  const { sm } = useMedia();
  const tokens = getTokens();
  const { height, width } = useWindowDimensions();

  const feature: {container: YStackProps, SvgContainer: YStackProps, Svg: SvgProps, Text: SizableTextProps} = {
    container: {
      alignItems:'center',
      // fontSize='$4'
      // fontWeight: '700',
      // margin:'5%',
    },
    SvgContainer: {
      marginBottom: '10%', 
      borderRadius: 50, 
      overflow: 'hidden' 
    },
    Svg: {
      height: sm ? 360 : '100%',
      width: sm ? 360 : '100%',
    },
    Text: {
      margin: 0,
      fontSize: '$4',
      fontWeight: '500',
      fontFamily: '$montserrat',
      color: '$appColor',
      $sm: {
        fontSize: '$3',
      }
    }
  }

  return (
    <ScrollView>
      <NavBar/>
      <YStack>
        <SvgBackground 
          type={sm ? "WaveHorizontalBrown" : "WaveVerticalBrown"}
          backgroundColor='$appBGColor2'
          height={Platform.OS === 'web' ? height - getVariableValue(tokens.size.navHeight) : height}
          width='100%'
        >
          <XStack 
            flex={1}
            $sm={{
              flexDirection: 'column',
            }}
          >
            <YStack
              overflow='hidden' 
              justifyContent='center'
              flex={1}
              flexBasis={0}
              paddingHorizontal='4%'
              paddingBottom={0}
              $sm={{
                paddingHorizontal: '2%',
                paddingBottom: '5%',
              }}
              minWidth={0}
            >
              <H2
                fontSize='$7'
                // lineHeight='$8'
                marginTop={0}
                marginBottom={0}
                color='$appColor'
                textAlign='center'
                $sm={{
                  fontSize: '$5',
                  lineHeight: 20,
                  marginTop: '5%',
                }}
              >
                Decentralized communication where you&apos;re the owner
              </H2>
              <Paragraph 
                marginTop='3%'
                fontSize='$5'
                lineHeight={40}
                color='$appColor'
                textAlign='center'
                $sm={{
                  fontSize: '$3',
                  lineHeight: 20,
                }}
              >
                We believe that private communication is the primary building block for making great connections with others. 
                Unfortunately, there aren&apos;t many options to choose from without watchful eyes on your data.
                The only way that we can change this is by challenging the status quo. 
                Hypha is taking on this difficult challenge by decentralizing communication as we know it.
              </Paragraph>
            </YStack>
            <YStack 
              alignItems='center'
              overflow='hidden'
              justifyContent='center'
              flex={1}
              flexBasis={0}
              paddingHorizontal='4%'
              paddingBottom={0}
              $sm={{
                paddingHorizontal: '2%',
                paddingBottom: '5%',
              }}
              minWidth={0}
            >
              <YStack 
                justifyContent='center'
                alignItems='center'
              >
                <XStack
                  alignItems='center'
                  overflow='hidden'
                >
                  <XStack
                    // height='66%'
                    // width='66%'
                  >
                {/* <MotiView
                  from={{ rotate: '0deg' }}
                  animate={{ rotate: '360deg' }}
                  transition={{
                    loop: true,
                    duration: 40000,
                    repeatReverse: false,
                    type: 'timing',
                    easing: Easing.linear
                  }}
                >
                <MotiView
                  from={{ scale: 1 }}
                  animate={{ scale: 1.1 }}
                  transition={{
                    loop: true,
                    duration: 3500,
                    repeatReverse: true,
                    type: 'timing',
                    easing: Easing.linear
                  }}
                > */}
                  <Logo.Hypha01 
                    height={sm ? width * .66 : width * .33}
                    width={sm ? width * .66 : width * .33}
                  />
                {/* </MotiView>
                </MotiView> */}
                  </XStack>
                </XStack>
              </YStack>
            </YStack>
          </XStack>
        </SvgBackground>
        <XStack 
          justifyContent='space-evenly'
          alignItems='center'
          backgroundColor='$appBGColor'
          height={height * .5}
          width='75%'
          margin='auto'
          paddingHorizontal='3%'
          paddingVertical='3%'
          $sm={{
            flexDirection: 'column',
            width: '100%',
            paddingHorizontal: 0,
            paddingVertical: '5%',
          }}
        >
          <YStack 
            justifyContent='space-evenly'
            alignItems='center'
            flex={1}
            height='33%'
            width='100%'
            paddingHorizontal={0}
            $sm={{
              paddingHorizontal: '5%',
            }}
          >
            <H1 
              fontSize='$6'
              textAlign='auto'
              paddingBottom={0}
              color='$appColor'
              $sm={{
                fontSize: '$5',
                textAlign: 'center',
                paddingBottom: '5%',
              }}
            >
              Have your friends arrived yet?
            </H1>
            <Input
              placeholder="Enter an Ethereum Address"
              value={inputValue}
              onChangeText={(text) => setInputValue(text)}
              onSubmitEditing={() => {
                if (inputValue !== "" && ethers.utils.isAddress(inputValue.trim())) {
                  // window.open(`/profile/${inputValue.trim()}`);
                  console.log('Key Pressed')
                }
              }}
              borderRadius={5}
              borderColor={ethers.utils.isAddress(inputValue.trim()) || inputValue.trim() === "" ? '$appColor' : 'red'}
              borderWidth={3}
              borderStyle='solid'
              color={ethers.utils.isAddress(inputValue.trim()) || inputValue.trim() === "" ? '$appColor' : 'red'}
              height='$6'
              width='66%'
              fontSize='$3'
              paddingLeft='$1'
            />
          </YStack>
          <YStack 
            flex={1}
          >
            <ProfileCard profile={{address: ''}} portrait={false}/>
          </YStack>
        </XStack>
        <YStack 
          flexDirection='column'
          justifyContent='center'
          alignItems='center'
          backgroundColor='$appBGColor'
        >
          <H1 
            fontSize='$6'
            marginTop='3%'
            textAlign='auto'
            color='$appColor'
            $sm={{
              fontSize: '$5',
              textAlign: 'center',
            }}
          >
            The Future of Communication
          </H1>
          <HGrid 
            columns={sm ? 1 : 3} 
            gap={'3%'}
            style={{
              margin: '3%',
              width: '75%',
            }} 
          >
            <YStack 
              {...feature.container}
            >
              <YStack 
                {...feature.SvgContainer}
              >
                <Patterns.LayeredSteps {...feature.Svg}/>
              </YStack>
              <SizableText 
                {...feature.Text}
              >
                Private
              </SizableText>
            </YStack>
            <YStack 
              {...feature.container}
            >
              <YStack 
                {...feature.SvgContainer}
              >
                <Patterns.CircleScatter {...feature.Svg}/>
              </YStack>
              <SizableText 
                {...feature.Text}
              >
                Decentralized
              </SizableText>
            </YStack>
            <YStack 
              {...feature.container}
            >
              <YStack 
                {...feature.SvgContainer}
              >
                <Patterns.LowPolyGrid {...feature.Svg}/>
              </YStack>
              <SizableText 
                {...feature.Text}
              >
                Data Ownership
              </SizableText>
            </YStack>
            <YStack 
              {...feature.container}
            >
              <YStack 
                {...feature.SvgContainer}
              >
                <Patterns.SymbolScatter {...feature.Svg}/>
              </YStack>
              <SizableText 
                {...feature.Text}
              >
                IPFS Storage
              </SizableText>
            </YStack>
            <YStack 
              {...feature.container}
            >
              <YStack 
                {...feature.SvgContainer}
              >
                <Patterns.LayeredWaves {...feature.Svg}/>
              </YStack>
              <SizableText 
                {...feature.Text}
              >
                Profiles
              </SizableText>
            </YStack>
            <YStack 
              {...feature.container}
            >
              <YStack 
                {...feature.SvgContainer}
              >
                <Patterns.BlobScene {...feature.Svg}/>
              </YStack>
              <SizableText 
                {...feature.Text}
              >
                Ethereum Login
              </SizableText>
            </YStack>
          </HGrid>
        </YStack>
        <YStack 
          flexDirection='column'
          justifyContent='space-evenly'
          alignItems='center'
          backgroundColor='$appBGColor2'
          minHeight={height * .3}
          paddingVertical='3%'
          paddingHorizontal='15%'
          $sm={{
            paddingVertical: '5%',
            paddingHorizontal: '5%',
          }}
        >
          <H1 
            fontSize='$6'
            textAlign='center'
            color='$appColor'
          >
            Powerful ethereum messaging at your fingertips
          </H1>
          <Paragraph 
            textAlign='center'
            lineHeight={22}
            fontSize='$4'
            marginHorizontal={0}
            color='$appColor'
            $sm={{
              marginHorizontal: '5%',
            }}
          >
            An Ethereum wallet is the only requirement to use Hypha. 
            Users can login and communicate in real-time with powerful instant messenger style features like typing indicators & online status. 
            Create profiles to more easily identify other users & groups. 
            All of the data created from interacting with Hypha is yours to control and decide the fate of!
          </Paragraph>
          <Link href="/app">
            <SizableText
              fontWeight='500'
              color='$inputBGColor'
              fontSize='$3'
              padding={10}
              borderStyle='solid'
              borderWidth={2}
              borderColor='$inputBGColor'
              whiteSpace='nowrap'
              hoverStyle={{
                color: '$inputHoverBGColor',
                backgroundColor: '$inputHoverColor',
              }}
            >
              Enter App
            </SizableText>
          </Link>
        </YStack>
      </YStack>
      <Footer/>
    </ScrollView>
  )
}
