import React, { useContext, useState } from 'react'
import { HButton, YStack } from '@hypha/ui'
import { ethers } from 'ethers'
import useWeb3 from 'app/components/hooks/useWeb3'
import { DispatchContext } from 'app/context/state'
import { Actions } from 'app/provider/state'
import { ConversationType } from 'app/services/Streamr_API'
import { MyceliumCreationModal } from 'app/components/MyceliumCreationModal'
import SettingsComponent from './SettingsComponent'
// import { Contract } from "ethers";
// import HyphaToken from "../../chain-info/HyphaToken.json"
// import { utils } from "ethers";
// import { useContractFunction, useContractCall } from "@usedapp/core";

// const HYPHA_ADDRESS = "0xe81FAE6d25b3f4A2bB520354F0dddF35bF77b21E";

export const Experimental = () => {
    const { provider } = useWeb3();
    const dispatch = useContext(DispatchContext);
    // const [messageGuessing, setMessageGuessing] = useState(false);

    // //HyphaToken Contract
    // const abi = HyphaToken;
    // const hyphaInterface = new utils.Interface(abi);
    // const contract = new Contract(HYPHA_ADDRESS, hyphaInterface)
    // const { send: sendRandomWinner } = useContractFunction(contract, 'randomWinner');
    // const { send: sendRandomNumber } = useContractFunction(contract, 'getRandomNumber');
  
    // const winner = useContractCall({
    //   abi: hyphaInterface,
    //   address: HYPHA_ADDRESS,
    //   method: "winner"
    // })

    // From /app addMessage()
    // if(messageGuessing){
    //   //Check HyphaToken for winner
    //   if(winner){
    //     console.log("Need to reset random number")
    //     sendRandomNumber();
    //   }
    //   //Select winner if number above random is guessed
    //   else{
    //     const guess = Math.floor(Math.random() * 100)
    //     await sendRandomWinner(guess);
    //     console.log("You guessed: " + guess);
    //   }
    // }

    // const userBalance = useTokenBalance(HYPHA_ADDRESS, account);
    
    // <br></br>
    // {account !== undefined && userBalance !== undefined ? 
    // ((ethers.utils.formatUnits(userBalance, 18) + " Hypha")) : ""}
    return (
        <SettingsComponent>
            <YStack
                borderWidth={1}
                borderColor='$appColor'
                borderStyle='solid'
                width='75%'
                marginHorizontal='auto'
                paddingVertical='$4'
            >
                {/* <button className="hypha-button" onClick={() => setMessageGuessing(!messageGuessing)}>{messageGuessing ? 
                "Participating in Message Guessing" : 
                "Not Participating in Message Guessing"}
                </button> */}
                <HButton
                    hypha='clean'
                    onPress={async () => {
                        provider.getSigner().signMessage('Sign a message')
                        .then(res => {
                            console.log(res);
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    }}
                >
                    <HButton.Text>
                        Sign Message
                    </HButton.Text>
                </HButton>
                <HButton
                    hypha='clean'
                    onPress={async () => {
                        // const hash = await provider.getSigner().signMessage('test signing a message');
                        // console.log(hash);
                        provider.getSigner().sendTransaction({
                            from: '0x98b01D04ab7B40Ffe856Be164f476a45Bf8E5B37', 
                            to: '0x98b01D04ab7B40Ffe856Be164f476a45Bf8E5B37', 
                            value: ethers.utils.parseEther('.01'), 
                        })
                        .then(res => {
                            console.log(res);
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    }}
                >
                    <HButton.Text>
                        Send Transaction
                    </HButton.Text>
                </HButton>
                <HButton
                    hypha='clean'
                    onPress={() => {
                            dispatch({
                                type: Actions.ADD_CONVERSATION, 
                                payload: { profile: [{ address: 'hypha' }], selected: false, streamId: 'Johns Hypha', type: ConversationType.Hypha }
                            });
                            dispatch({
                              type: Actions.ADD_CONVERSATION, 
                              payload: { profile: [{ address: 'hyphae' }], selected: false, streamId: 'Johns Hyphae', type: ConversationType.Hyphae }
                            });
                            dispatch({
                              type: Actions.ADD_CONVERSATION, 
                              payload: { profile: [{ address: 'mycelium' }], selected: false, streamId: 'Johns Mycelium', type: ConversationType.Mycelium }
                            });
                    }}
                >
                    <HButton.Text>
                        Add Fake Conversations
                    </HButton.Text>
                </HButton>
                <MyceliumCreationModal
                    hypha='clean'
                    // create={async (name: string) => await createMycelium(name)}
                    create={() => {}}
                    cancel={() => {}}
			    />
            </YStack>
        </SettingsComponent>
    )
}
