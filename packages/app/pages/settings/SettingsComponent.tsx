import React from 'react'
import SafeArea from "app/components/SafeArea";

/**
 * Wraps each settings component with SafeArea & provider the same props
 */
export default function SettingsComponent({ children } : { children: React.ReactNode }) {
    return (
        <SafeArea paddingVertical='$4'>
            { children }
        </SafeArea>
    )
}
