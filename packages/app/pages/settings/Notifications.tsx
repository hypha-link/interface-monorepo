import React from "react";
import { Fieldset, Label, Spacer, Switch, YStack } from "@hypha/ui";
import SettingsComponent from "./SettingsComponent";

export const Notifications = () => {
  return (
    <SettingsComponent>
        <YStack ai="center">
            <Fieldset flexDirection="row" borderColor='transparent'>
                <Label htmlFor="Notifications">Notifications</Label>
                <Spacer/>
                <Switch id="Notifications" borderColor='$appColor' focusStyle={{ borderColor: '$appColor', outlineColor: 'transparent' }}>
                    <Switch.Thumb backgroundColor='$appColor' animation='quick' />
                </Switch>
            </Fieldset>
        </YStack>
    </SettingsComponent>
  );
};