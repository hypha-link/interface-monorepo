import React, { useContext, useState } from 'react'
import { StateContext } from 'app/context/state'
import getShortAddress from '../../get/getShortAddress'
import ProfileImage from '../../components/ProfileImage'
import { getVariableValue, SizableText, SizableTextProps, Spacer, useMedia, useTheme, XStack, XStackProps, YStack } from '@hypha/ui'
import { Edit } from '@hypha/ui/components'
import getGrayscaleHex from 'app/get/getGrayscaleHex'
import { getDocumentAsync } from 'expo-document-picker'
import SettingsComponent from './SettingsComponent'
// import { createHelia } from 'helia'
// import { unixfs } from '@helia/unixfs'
// import RNBU from 'react-native-blob-util'

type fileType = {
  type: "cancel" | "success";
  name: string;
  size?: number;
  uri: string;
  mimeType?: string;
  lastModified?: number;
  file?: File;
  output?: FileList;
}

export const Profile = () => {
  const { selfId, ownProfile } = useContext(StateContext);
  const [hover, setHover] = useState(false);
  const [file, setFile] = useState<fileType>()
  const { sm } = useMedia();

  // const changeImage = async (newImage: string) => {
  //     await selfId.merge('basicProfile', {
  //         image: {
  //             original: {
  //                 src: newImage,
  //                 mimeType: "image/png",
  //             },
  //         },
  //     });
  // }

  // async function uploadImage(selectedFile: fileType) {
  //   const helia = await createHelia();
  //   const fs = unixfs(helia);
  //   await fs.addFile(
  //     {
  //       content: RNBU.wrap(selectedFile.uri),
  //       path: selectedFile.name
  //     },
  //     {
  //       wrapWithDirectory: true
  //     }
  //   ).then((res) => {
  //     changeImage(`https://ipfs.io/ipfs/${res.cid.toString()}/${selectedFile.name}`);
  //     console.log(res);
  //   })
  // }

  const styles: { container: XStackProps, text: SizableTextProps, subContainer: XStackProps } = {
    container: {
      jc: 'flex-start',
      ai: 'center',
      width: '100%',
      paddingVertical: '$3',
      paddingHorizontal: '$5',
    },
    text: {
      fontSize: '$4',
      fontWeight: '$4',
      // borderBottomColor: '$appColor',
      // borderBottomWidth: 1,
    },
    subContainer: {
      flex: 1,
      jc: 'center',
      ai: 'flex-end',
    }
  }

  return (
    <SettingsComponent>
      <YStack
        flexBasis='100%'
        jc='center'
        ai='center'
      >
        <YStack
          width='100%'
          height='100%'
          ai='flex-start'
        >
          <XStack {...styles.container}>
            <SizableText {...styles.text}>
              Username
            </SizableText>
            <YStack {...styles.subContainer}>
              {/* <Paragraph>{ownProfile?.name ? ownProfile.name : getShortAddress(ownProfile?.address)}</Paragraph> */}
              <Edit type='name' />
            </YStack>
          </XStack>
          <XStack {...styles.container}>
            <SizableText {...styles.text}>
              Description
            </SizableText>
            <YStack {...styles.subContainer}>
              {/* <Paragraph>{ownProfile && ownProfile?.description ? ownProfile.description : ''}</Paragraph> */}
              <Edit type='description' />
            </YStack>
          </XStack>
          <XStack {...styles.container}>
            <SizableText {...styles.text}>
              Picture
            </SizableText>
            <Spacer />
            <ProfileImage
              profile={ownProfile}
              override={file?.uri || 'https://ipfs.io/ipfs/QmcrbG1oUYbDYYsWhiSxwBXszakMaw14ev8Gvz73xbG45c'}
              sizePx={150}
              fill='$appColor'
              grayscale={hover}
              animation={'medium'}
              onHoverIn={() => setHover(true)}
              onHoverOut={() => setHover(false)}
              // SM because causes issues with onPress activating on web (onPressOut)
              onPressIn={() => sm && setHover(true)}
              onPressOut={() => sm && setHover(false)}
              onPress={() => {
                getDocumentAsync({ type: 'image/*', multiple: true }).then(selectedFile => {
                  if (selectedFile.type === 'success') {
                    setFile(selectedFile)
                    console.log(selectedFile)
                  }
                  else if (selectedFile.type === 'cancel') {
                    console.info('User cancelled action')
                  }
                  else {
                    console.warn('Failed to retrieve file')
                  }
                })
              }}
            >
              <SizableText
                position='absolute'
                bottom={0}
                backgroundColor={hover ? getGrayscaleHex(`${getVariableValue(useTheme().uiHoverColor)}bf`) : `${getVariableValue(useTheme().uiHoverColor)}bf`}
                width='100%'
                opacity={hover ? 100 : 0}
                animation={'slow'}
                color='$appBGColor'
                fontWeight='bold'
                textAlign='center'
                userSelect='none'
                zIndex='$1'
              >
                Change
              </SizableText>
            </ProfileImage>
          </XStack>
        </YStack>
      </YStack>
    </SettingsComponent>
  )
}
