import React from "react";
import { Linking } from "react-native";
import { HButton, YStack } from "@hypha/ui";
import SettingsComponent from "./SettingsComponent";

export const ManageData = () => {
  return (
    <SettingsComponent>
      <YStack>
        <HButton
          hypha='clean'
          onPress={() => Linking.openURL("https://streamr.network/core")}
        >
          <HButton.Text fontSize='$4' fontWeight='$4'>
            Access your data here!
          </HButton.Text>
        </HButton>
      </YStack>
    </SettingsComponent>
  );
};
