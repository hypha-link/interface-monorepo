import React from "react";
import { HButton, YStack } from "@hypha/ui";
import SettingsComponent from "./SettingsComponent";

export const Appearance = () => {
    return (
        <SettingsComponent>
            <YStack>
                <HButton
                    hypha='clean'
                    disabled
                >
                    <HButton.Text>
                        Language
                    </HButton.Text>
                </HButton>
                <HButton
                    hypha='clean'
                >
                    <HButton.Text>
                        Theme
                    </HButton.Text>
                </HButton>
                <HButton
                    hypha='clean'
                    disabled
                >
                    <HButton.Text>
                        Message font size
                    </HButton.Text>
                </HButton>
                <HButton
                    hypha='clean'
                    disabled
                >
                    <HButton.Text>
                        Navigation font size
                    </HButton.Text>
                </HButton>
            </YStack>
        </SettingsComponent>
    );
};