import React, { useRef, useState } from 'react';
import { default as OnboardingBase, Page } from 'react-native-onboarding-swiper';
import { useRouter } from 'solito/router';
import HomeNavButton from 'app/components/HomeNavButton';
import {
	Button, 
	getConfig, 
	getTokens, 
	getVariableValue, 
	SizableText, 
	useTheme, 
	useWindowDimensions, 
	YStack, 
	YStackProps, 
} from '@hypha/ui';

import { Patterns, SvgProps } from '@hypha/ui/components'

export default function Onboarding() {
    const { replace } = useRouter();
	const { height, width } = useWindowDimensions();
	const onboardingRef = useRef<OnboardingBase>(null);
	const [currentPage, setCurrentPage] = useState(0);
	const tokens = getTokens();
	const theme = useTheme();
	
	const pageIndexCallback = (pageIndex: number) => setCurrentPage(pageIndex + 1);

	const onboardingStyles: {container: YStackProps, svgContainer: YStackProps, svg: SvgProps} = {
		container: {

		},
		svgContainer: {
			borderRadius: 50, 
			overflow: 'hidden', 
			height: width * .7,
			width: width * .7, 
		},
		svg: {
			height: width * .7, 
			width: width * .7
		},
	}

	const pages: Page[] = [
		{
			backgroundColor: '#FFF',
			image: <HomeNavButton type='LogoOnly' style={{height: height * .5, width: width * .5}}/>,
			title: 'HYPHA',
			subtitle: "We believe that private communication is the primary building block for making great connections with others. Unfortunately, there aren't many options to choose from without watchful eyes on your data. The only way that we can change this is by challenging the status quo. Hypha is taking on this difficult challenge by decentralizing communication as we know it.",
		},
		{
			backgroundColor: getVariableValue(tokens.color.pear),
			image: (
				<YStack {...onboardingStyles.svgContainer}>
					<Patterns.LayeredSteps {...onboardingStyles.svg}/>
				</YStack>
			),
			title: 'Private & Decentralized',
			subtitle: 'Message & communicate with your peers like never before. Join the decentralized landscape for superior privacy & best-in-class uptime.',
		},
		{
			backgroundColor: getVariableValue(tokens.color.cannon),
			image: (
				<YStack {...onboardingStyles.svgContainer}>
					<Patterns.LowPolyGrid {...onboardingStyles.svg}/>
				</YStack>
			),
			title: 'Data Ownership & IPFS Storage',
			subtitle: 'Control your data at your own discretion & assume rightful ownership.',
		},
		{
			backgroundColor: '#FFF',
			image: (
				<YStack {...onboardingStyles.svgContainer}>
					<Patterns.BlobScene {...onboardingStyles.svg}/>
				</YStack>
			),
			title: 'Profiles & Ethereum Login',
			subtitle: 'Profiles for each and every user based on your ethereum address. Super easy & quick passwordless login for fast access to anything!',
		},
	];

	return (
		<>
			<OnboardingBase
				onDone={() => replace('/app')}
				bottomBarHeight={getVariableValue(tokens.size.navHeight)}
				bottomBarHighlight={false}
				showSkip={false}
				showDone={false}
				ref={onboardingRef}
				titleStyles={{
					color: getVariableValue(theme.appColor),
					fontFamily: getVariableValue(getConfig().fonts['Alternates'])
				}}
				subTitleStyles={{
					color: getVariableValue(theme.appColor),
					fontFamily: getVariableValue(getConfig().fonts['Montserrat'])
				}}
				pageIndexCallback={pageIndexCallback}
				showPagination={currentPage !== pages.length}
				pages={pages}
			/>	
			<Button 
				onPress={() => replace('/app')}
				display={currentPage === pages.length ? 'flex' : 'none'}
				height='$navHeight'
				width='100%'
				color='$cannon'
				justifyContent='center'
				alignItems='center'
				backgroundColor='$pear'
				hoverStyle={{
					opacity: .75,
				}}
				pressStyle={{
					opacity: .75,
				}}
			>
				<SizableText 
					fontFamily='$alternates'
					fontSize='$6'
					color='$cannon'
					// from={{ translateX: -30 }}
					// animate={{ translateX: 30 }}
					// transition={{
					//   loop: true,
					//   duration: 1000,
					//   repeatReverse: true,
					//   type: 'timing',
					//   easing: Easing.linear
					// }}
				>
					➤
				</SizableText>
			</Button>
		</>
	);
}
