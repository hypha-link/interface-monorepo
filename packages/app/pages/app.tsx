import React, { useContext, useEffect, useState } from "react";

import { useEthers } from "@usedapp/core";
// import { create } from "ipfs-core";

import { Message } from "app/components/Message";
import { SendMessage } from "app/components/SendMessage";

import { EthereumAuthProvider, SelfID } from '@self.id/web';
import getOrCreateMessageStream, { ConversationType } from "app/services/Streamr_API"
import { Conversations, MessagePayload, Metadata } from "app/components/utils/Types";
// import usePublishMetadata from "app/components/hooks/usePublishMetadata";
import { Stream, StreamPermission } from "streamr-client";
import useConversationStorage, { localStreamKey } from "app/components/hooks/useConversationStorage"

import { DispatchContext, StateContext } from "app/context/state";
import { Actions } from "app/provider/state";
import getConversationProfile from "app/get/getConversationProfile";
import getProfilePicture from "app/get/getProfilePicture";
import { ethers } from "ethers";
import useStreamrSession from 'app/components/hooks/useStreamrSession';
import useSelectedConversation from "app/components/hooks/useSelectedConversation";
import Head from "next/head";
// import { Occlusion } from "app/provider/Occlusion";
import { OcclusionContext } from "app/context/occlusion";
import HomeNavButton from "app/components/HomeNavButton";
import Sidebar from "app/components/Sidebar";
import { useMessaging } from "app/services/messaging";
import { useConversations } from "app/services/conversations"
import { 
  Button, 
  ButtonProps, 
  getTokens, 
  getVariableValue, 
  HButton, 
  Input, 
  ScrollView, 
  SizableText, 
  useMedia, 
  useTheme, 
  useWindowDimensions, 
  XStack, 
  YStack, 
  YStackProps 
} from "@hypha/ui";
import { Platform, StatusBar } from "react-native";
import { NavBar } from 'app/components/AppNavBar'
import SafeArea from "app/components/SafeArea";
import { DrawerProps } from "app/navigation/native";

export default function App({ route, navigation }: DrawerProps) {
  const { activateBrowserWallet } = useEthers();
  const tokens = getTokens();

  //Global State
  const { selfId, notifications, conversations, streamr, streamrDelegate, ownProfile, web3Provider } = useContext(StateContext);
  const { address: ownAddress } = ownProfile || {};
  const dispatch = useContext(DispatchContext);
  // const occludedElements = useContext(OcclusionContext);
  const { height } = useWindowDimensions();
  const { sm } = useMedia();

  // //Component Constructors
  const [conversationModal, setConversationModal] = useState<string>(undefined);
  const [searchKey, setSearchKey] = useState<string>('');

  const selectedConversation = useSelectedConversation();
  // const [setMetadata] = usePublishMetadata(selectedConversation);
  const { ceramicConversations, ceramicStream } = useConversationStorage();

  // useStreamrSession();

  const MessagingOrchestrator = useMessaging();
  const ConversationOrchestrator = useConversations();

  // useEffect(() => {
  //   const connect = async () => {
  //     try{
  //       const selfIdClient = await SelfID.authenticate({
  //         authProvider: new EthereumAuthProvider(window.ethereum, ownAddress),
  //         ceramic: 'testnet-clay',
  //         connectNetwork: 'testnet-clay',
  //       })
  //       dispatch({ type: Actions.SET_SELFID, payload: selfIdClient });
  //       dispatch({ type: Actions.SET_PROFILE, payload: {address: ownAddress, ...await selfIdClient.get('basicProfile')} });
  //       dispatch({ type: Actions.SET_IPFS, payload: await create({ repo: "uploaded-files"})});
  //     }
  //     catch{
  //       console.log("User needs an Ethereum wallet to connect to Hypha.");
  //     }
  //   }
  //   if(ownAddress){
  //     connect();
  //     // if(web3Provider) web3Provider.getSigner().signMessage('Welcome to Hypha');
  //   }
  // }, [ownAddress])

  //Connects ethereum wallet to Hypha
  const connect = async () => {
    try{
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      provider.send("eth_requestAccounts", []);
      const signer = provider.getSigner();
      activateBrowserWallet();
      dispatch({ type: Actions.SET_WEB3_PROVIDER, payload: provider });
      dispatch({ type: Actions.SET_ACCOUNT, payload: await signer.getAddress() });
      console.info("The client attempted to connect");
    }
    catch(e){
      alert('Please enable your wallet to connect to Hypha.');
      console.error(`Could not connect to wallet: ` + e);
    }
  };

  //Disconnects wallet and removes all data from window
  const disconnect = async () => {
    console.info("The client has been disconnected");
    notifications.forEach(notification => {
      notification.close();
    });
    setSearchKey('');
    // setMetadata(undefined);
    dispatch({ type: Actions.CLEAR_STATE })
  };

  useEffect(() => {
    async function loadConversations(){
      //Add conversation profiles & initial metadata to object
      const newConversations = await Promise.all(ceramicConversations.map(async (conversation) => {
        const newProfile = await Promise.all(conversation.profile.map(async (profile) => {
          try{
            //Retrieve the DID address associated with this ethereum address
            const didAddress = await selfId.client.getAccountDID(`${profile.address}@eip155:${web3Provider.network.chainId}`);
            //Return the basicProfile associated with this DID address
            return {address: profile.address, ...await selfId.client.get('basicProfile', didAddress)}
          }
          catch(e){
            console.warn(`There is no DID that exists for ${profile.address}`);
            console.log(e);
          }
        }))
        //Returns the conversation with a profile & initial metadata
        return {...conversation, profile: newProfile, metadata: {address: '', typing: false, online: false, receipt: false, invite: ''}};
      }));
      //Set valid streams if any conversations don't have one
      if(newConversations.some(conversation => conversation.streamId === '' || conversation.streamId === undefined)) await ConversationOrchestrator({ type: "SET_ALL_VALID_STREAMS", payload: newConversations });
      //Subscribe if conversations don't have empty/null streamIds
      if(newConversations.filter(conversation => conversation.streamId === '' || conversation.streamId === undefined).length === 0) await ConversationOrchestrator({ type: "SUBSCRIBE_TO_CONVERSATIONS", payload: newConversations });
      dispatch({ type: Actions.SET_CONVERSATIONS, payload: newConversations });
    }
    if(selfId && ceramicConversations?.length > 0 && streamr){
      loadConversations();
      // setMetadata(oldMetadata => ({...oldMetadata, online: true}));
    }
  }, [selfId, ceramicConversations, streamr])

  // Load messages on startup
  // useEffect(() => {
  //   async function loadMessages() {
  //     try {
  //       let timeoutID: NodeJS.Timeout;
  //       let messageArr: MessagePayload[];
  //       const stream = await streamr.getStream(selectedConversation.streamId);
  //       const storageNodes = await stream.getStorageNodes();
  //       //Load the last 50 messages from previous session if messages are being stored
  //       if(storageNodes.length !== 0){
  //         await streamr.resend(
  //             selectedConversation.streamId,
  //             {
  //               last: 50,
  //             },
  //             (message: MessagePayload) => {
  //               //Collect all data
  //               messageArr.push(message);
  //               //Reset timer if all data hasn't been gathered yet
  //               if(timeoutID)
  //               clearInterval(timeoutID);
  //               timeoutID = setTimeout(() => {
  //                 //Load messages after all data has been collected
  //                 dispatch({ type: Actions.SET_MESSAGES, payload: {conversation: selectedConversation, messages: messageArr} })
  //               }, 100);
  //             }
  //           )
  //       }
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }
  //   //Load if the user wallet is connected
  //   if(account && selectedConversation.messages.length === 0  && selectedConversation.streamId !== ""){
  //     loadMessages();
  //   }
  // }, [account, conversations.find(conversation => conversation.selected)])

  const testFn = async () => {
    try{
      console.info('Notifications')
      // console.log(await streamr.getStream('0x98b01d04ab7b40ffe856be164f476a45bf8e5b37/hypha/0x92B188a4Db0E5a8475b3595f2A63188AF2AfAb16'));
      // console.log(await streamr.getStream(selectedConversation.streamId));
    }
    catch(e){
      console.error(e);
    }
  }

  return (
    <SafeArea>
      <StatusBar barStyle="light-content" backgroundColor={getVariableValue(tokens.color.cannon)} animated={true}/>
      <ScrollView>
        {/* Occluded Elements */}
        {/* {
          occludedElements ?
          <Occlusion>
            {occludedElements}
          </Occlusion>
          :
          <></>
        } */}
        {/* Top Bar */}
        <NavBar
          test={() => testFn()}
          toggleDrawer={() => navigation.toggleDrawer()}
        />
        {/* Content Section */}
        <XStack 
          flexDirection='row'
          backgroundColor='$appBGColor'
          // color='appColor'
        >
          {
            !sm && <Sidebar/>
          }
          <YStack 
            justifyContent='flex-end'
            backgroundColor='transparent'
            height={height - getVariableValue(tokens.size.navHeight)}
            flex={3}
          >
            <YStack 
              flexDirection='column-reverse'
              flex={1}
              // overflowY='auto'
              // visibility='hidden'
            >
              <YStack 
                justifyContent='flex-end'
                backgroundColor='transparent'
                height={'100%'}
                flex={3}
              >
                {
                  selectedConversation.messages?.filter(e => e.message.includes(searchKey)) && 
                  selectedConversation.messages?.filter(e => e.message.includes(searchKey)).map((message: MessagePayload) => {
                    return (
                      <Message
                        key={Math.random()}
                        payload={message}
                        selectMessage={(message: MessagePayload) => MessagingOrchestrator('selectMessage', message)}
                        deleteMessage={(message: MessagePayload) => MessagingOrchestrator('deleteMessage', message)}
                      />
                    );
                  })
                }
              </YStack>
            </YStack>
            <SendMessage
              disable={!selectedConversation.selected}
              // typing={(typing: boolean) => setMetadata(oldMetadata => ({...oldMetadata, typing: typing}))}
              typing={() => false}
              sendMessage={(messageText: string) => MessagingOrchestrator('addMessage', {sender: ownAddress, message: messageText, date: new Date().toString()})}
            />
          </YStack>
        </XStack>
      </ScrollView>
    </SafeArea>
  );
}