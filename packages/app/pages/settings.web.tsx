import React, { useState } from 'react'
import { Profile } from "app/pages/settings/Profile";
import { ManageData } from 'app/pages/settings/ManageData';
import { Experimental } from 'app/pages/settings/Experimental';
import { Appearance } from 'app/pages/settings/Appearance';
import { Notifications } from 'app/pages/settings/Notifications';
import { useRouter } from 'solito/router';
import { H3, XStack, YStack, YStackProps, HButton, SizableTextProps, HButtonProps } from '@hypha/ui';

type tabsType = 
| 'profile'
| 'manageData'
| 'appearance'
| 'notifications'
| 'experimental'

export default function Settings() {
    const [tab, setTab] = useState<tabsType>('profile');
    const { back } = useRouter();

    const selectedTab = () => {
        switch(tab){
            case 'profile':
                return <Profile/>
            case 'manageData':
                return <ManageData/>
            case 'appearance':
                return <Appearance/>
            case 'notifications':
                return <Notifications/>
            case 'experimental':
                return <Experimental/>
        }
    }

    const styles: {header: SizableTextProps, menuContainer: YStackProps, menu: HButtonProps, selectedMenu: HButtonProps} = {
        header: {
            fontSize: '$6',
            paddingVertical: '5%',
        },
        menuContainer: {
            paddingBottom: '3%',
            paddingRight: '10%',
            borderBottomColor: '$uiColor',
            borderBottomWidth: 2,
        },
        menu: {
            userSelect: 'none',
            justifyContent: 'flex-start',
            size: 30
        },
        selectedMenu: {
            backgroundColor: '$pear',
            focusStyle: {
                backgroundColor: '$pear',
            },
            hoverStyle: {
                backgroundColor: '$pear',
            }
        }
    }

    return (
        <XStack 
            flexDirection='row'
            jc='space-evenly'
            width='100%'
            height='100%'
            backgroundColor='$appBGColor'
            paddingHorizontal='5%'
            paddingTop='5%'
        >
            <YStack 
                flexDirection='column'
                alignItems='center'
                flexBasis={'33%'}
            >
                <H3 {...styles.header}>
                    User Settings
                </H3>
                <YStack {...styles.menuContainer}>
                    <HButton 
                        onPress={() => setTab('profile')}
                        {...tab === 'profile' && styles.selectedMenu}
                        {...styles.menu}
                    >
                        <HButton.Text>
                            Profile
                        </HButton.Text>
                    </HButton>
                    <HButton 
                        onPress={() => setTab('manageData')}
                        {...tab === 'manageData' && styles.selectedMenu}
                        {...styles.menu}
                    >
                        <HButton.Text>
                            Manage Data
                        </HButton.Text>
                    </HButton>
                </YStack>
                <H3 {...styles.header}>App Settings</H3>
                <YStack 
                    {...styles.menuContainer}
                >
                    <HButton 
                        onPress={() => setTab('appearance')}
                        {...tab === 'appearance' && styles.selectedMenu}
                        {...styles.menu}
                    >
                        <HButton.Text>
                            Appearance
                        </HButton.Text>
                    </HButton>
                    <HButton 
                        onPress={() => setTab('notifications')}
                        {...tab === 'notifications' && styles.selectedMenu}
                        {...styles.menu}
                    >
                        <HButton.Text>
                            Notifications
                        </HButton.Text>
                    </HButton>
                    <HButton 
                        onPress={() => setTab('experimental')}
                        {...tab === 'experimental' && styles.selectedMenu}
                        {...styles.menu}
                    >
                        <HButton.Text>
                            Experimental
                        </HButton.Text>
                    </HButton>
                </YStack>
            </YStack>
            <YStack 
                position='relative'
                flexBasis='33%'
                paddingRight='$5'
                paddingVertical='$4'
            >
                <HButton
                    hypha='standard'
                    onPress={() => back()}
                    position='absolute'
                    top={0}
                    right={0}
                    size={30}
                    zIndex='$1'
                >
                    <HButton.Text
                        color='$pear'
                    >
                        X
                    </HButton.Text>
                </HButton>
                {selectedTab()}
            </YStack>
        </XStack>
    )
}
