import { HButton, YStack } from '@hypha/ui'
import React from 'react'
import { useRouter } from 'solito/router'
import SafeArea from 'app/components/SafeArea';

export default function Settings() {
    const { replace } = useRouter();
    return (
        <SafeArea>
            <YStack>
                <HButton
                    onPress={() => replace('/profile')}
                >
                    <HButton.Text>
                        Profile
                    </HButton.Text>
                </HButton>
                <HButton
                    onPress={() => replace('/data')}
                >
                    <HButton.Text>
                        Manage Data
                    </HButton.Text>
                </HButton>
                <HButton
                    onPress={() => replace('/appearance')}
                >
                    <HButton.Text>
                        Appearance
                    </HButton.Text>
                </HButton>
                <HButton
                    onPress={() => replace('/notifications')}
                >
                    <HButton.Text>
                        Notifications
                    </HButton.Text>
                </HButton>
                <HButton
                    onPress={() => replace('/experimental')}
                >
                    <HButton.Text>
                        Experimental
                    </HButton.Text>
                </HButton>
            </YStack>
        </SafeArea>
    )
}
