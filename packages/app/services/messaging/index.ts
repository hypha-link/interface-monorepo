// ORCHESTRATOR

import useSelectedConversation from 'app/components/hooks/useSelectedConversation';
import { Conversations, MessagePayload } from 'app/components/utils/Types';
import { StateContext } from 'app/context/state'
import { GlobalState } from 'app/provider/state';
import React, { useContext } from 'react'
import { addMessage } from './addMessage'
import { selectMessage } from './selectMessage'
import { deleteMessage } from './deleteMessage'

type type = "addMessage" | "selectMessage" | "deleteMessage"

export type StateIntersect = GlobalState & { selectedConversation: Conversations }

export function useMessaging() {
    const state = useContext(StateContext);
    const selectConversation = useSelectedConversation();
    const stateIntersect = {...state, selectedConversation: selectConversation};

    async function Orchestrator(type: type, message: MessagePayload) {
        switch(type){
            case "addMessage":
                return addMessage(stateIntersect, message)
            case "selectMessage":
                return selectMessage(stateIntersect, message)
            case "deleteMessage":
                return deleteMessage(stateIntersect, message)
        }
    }

    return Orchestrator;
}
