import { MessagePayload } from "app/components/utils/Types";
import { StreamPermission } from "streamr-client";
import { StateIntersect } from ".";

  //Publish a message to stream
export async function addMessage(stateIntersect: StateIntersect, _message: MessagePayload) {
    const { streamr, streamrDelegate, selectedConversation } = stateIntersect;
    try{
        //Grant publish permissions to the delegate if it doesn't have them already
        if(!await streamr.isStreamPublisher(selectedConversation.streamId, streamrDelegate?.wallet.address)){
        await streamr.grantPermissions(selectedConversation.streamId, {
            user: streamrDelegate?.wallet.address,
            permissions: [StreamPermission.PUBLISH],
        })
        }
        await streamrDelegate?.client.publish(
        {streamId: selectedConversation.streamId, partition: 0},
        {
            sender: _message.sender,
            message: _message.message,
            date: _message.date
        }
        )
    }
    catch(e){
        alert('Please fund the connected wallet with Matic tokens to use Hypha');
        console.error(e);
    }
};