import AsyncStorage from "@react-native-async-storage/async-storage";
import { localStreamKey } from "app/components/hooks/useConversationStorage";
import { Conversations } from "app/components/utils/Types";
import getConversationProfile from "app/get/getConversationProfile";
import { StateIntersect } from ".";
import { ConversationType } from "../Streamr_API";
import { getValidStream } from "./getValidStream";

export async function addConversations(stateIntersect: StateIntersect, _address: string){
    const { ceramicConversations, ceramicStream, selfId, ownProfile } = stateIntersect;
    const { address: ownAddress } = ownProfile || {};

    //Check if conversation exists already
    if(!ceramicConversations?.some(conversation => getConversationProfile(conversation).address === _address)){
        const validStream = await getValidStream(
            stateIntersect,
            {
                profile: [{ address: _address }], 
                streamId: undefined, 
                selected: false, 
                type: ConversationType.Hypha 
            }
        );
        console.log(validStream);
        const newConversation: Conversations = {
        profile: [{ address: _address }],
        streamId: validStream.id,
        selected: false,
        type: ConversationType.Hypha,
        }
        //Create a new conversations stream if there are no previously existing streams & pin it
        if(ceramicConversations && ceramicStream){
            await ceramicStream.update({conversations: [...ceramicConversations, newConversation]})
        }
        else{
        const stream = await selfId.client.tileLoader.create(
            {
                conversations: [newConversation]
            },
            {
                tags: ['conversations'],
            },
            {
                pin: true,
            }
        );
        //Add a new local storage record of the stream ID
        await AsyncStorage.setItem(`${ownAddress}-${localStreamKey}`, stream.id.toString());
        }
        return newConversation;
    }
}