import { Conversations, MessagePayload, Metadata } from "app/components/utils/Types";
import getProfilePicture from "app/get/getProfilePicture";
import { Actions, GlobalDispatch } from "app/provider/state";
import { StateIntersect } from ".";

export const subscribeToConversations = async (stateIntersect: StateIntersect, dispatch: GlobalDispatch, _conversations: Conversations[]) => {
    const { streamr, ownProfile } = stateIntersect;
    const { address: ownAddress } = ownProfile || {};

    const subs = await streamr.getSubscriptions();
    //Subscribe to stream after messages were resent
    await Promise.all(_conversations.map(async (conversation) => {
      console.log(subs);
      //Check if conversation streamId is empty & check if we are already subscribed
      if(conversation.streamId !== "" && subs.filter(sub => sub.streamPartId.substring(0, sub.streamPartId.indexOf('#')) === conversation.streamId).length === 0){
        console.info('Subscribed to ' + conversation.streamId);
        await streamr.subscribe(
          {
            stream: conversation.streamId,
            partition: 0,
          }, (data: MessagePayload) => {
            //Create a new notification if the new message was not sent by us & interface is not visible
            if(data.sender !== ownAddress && document.visibilityState !== "visible"){
              //Get the profile of the user that sent the message
              const senderProfile = conversation.profile.find(_profile => _profile.address === data.sender);
              const name = senderProfile?.name ? senderProfile?.name : data.sender;
              const image = getProfilePicture(senderProfile).image;
              const notification = new Notification(`${name} sent you a message!`, {body: data.message, icon: image});
              dispatch({ type: Actions.ADD_NOTIFICATION, payload: notification });
            }
            dispatch({ type: Actions.ADD_MESSAGE, payload: {conversation: conversation, message: data}});
          }
        )
        await streamr.subscribe(
          {
            stream: conversation.streamId,
            partition: 1,
          }, (data: Metadata) => {
            if(data.address !== ownProfile.address){
              dispatch({ type: Actions.SET_METADATA, payload: {conversation: conversation, metadata: data}});
              console.log(data.invite);
              if(data?.invite){
                // setConversationModal(data.invite);
              }
            }
          }
        )
      }
    }))
}