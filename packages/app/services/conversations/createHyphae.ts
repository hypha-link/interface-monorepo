import AsyncStorage from "@react-native-async-storage/async-storage";
import { localStreamKey } from "app/components/hooks/useConversationStorage";
import { Conversations } from "app/components/utils/Types";
import { StateIntersect } from ".";
import getOrCreateMessageStream, { ConversationType } from "../Streamr_API";

export async function createHyphae(stateIntersect: StateIntersect, ){
    const { ceramicConversations, ceramicStream, streamr, ownProfile, selfId } = stateIntersect;
    const { address: ownAddress } = ownProfile;

    const id = Math.floor(Math.random()*16777215).toString(16);
    console.log('Create Hyphae ' + id);

    //Check if conversation exists already
    if(!ceramicConversations?.some(conversation => conversation.streamId.includes(id))){
      const stream = await getOrCreateMessageStream(streamr, id, ConversationType.Hyphae, false);
      const newConversation: Conversations = {
        profile: [{ address: id }],
        streamId: stream.id,
        selected: false,
        type: ConversationType.Hyphae,
      }
      //Create a new conversations stream if there are no previously existing streams & pin it
      if(ceramicConversations && ceramicStream){
        await ceramicStream.update({conversations: [...ceramicConversations, newConversation]})
      }
      else{
        const stream = await selfId.client.tileLoader.create(
          {
            conversations: [newConversation]
          },
          {
            tags: ['conversations'],
          },
          {
            pin: true,
          }
        );
        //Add a new local storage record of the stream ID
        await AsyncStorage.setItem(`${ownAddress}-${localStreamKey}`, stream.id.toString());
      }
      return newConversation;
    }
}