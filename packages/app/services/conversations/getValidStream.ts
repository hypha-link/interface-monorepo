import { Conversations } from "app/components/utils/Types";
import getConversationProfile from "app/get/getConversationProfile";
import { Stream } from "streamr-client";
import { StateIntersect } from ".";
import getOrCreateMessageStream, { ConversationType } from "../Streamr_API";

export const getValidStream = async (stateIntersect: StateIntersect, _conversation: Conversations) => {
    const { ownProfile, streamr } = stateIntersect;
    const { address: ownAddress } = ownProfile || {};

    if(_conversation.type !== ConversationType.Hypha) return;

    try{
      const streams: Stream[] = [];
      //Check if either conversation has a stream
      for await(const stream of streamr.searchStreams(getConversationProfile(_conversation).address, { user: ownAddress, allowPublic: true })){
        streams.push(stream);
      }
      for await(const stream of streamr.searchStreams(ownAddress, { user: getConversationProfile(_conversation).address, allowPublic: true })){
        streams.push(stream);
      }
      //Return found stream
      if(streams.length !== 0) return streams[0];
      //Create a new stream since none were found
      const newStream = await getOrCreateMessageStream(streamr, getConversationProfile(_conversation).address, ConversationType.Hypha, false);
      return newStream;
    }
    catch(e){
      console.warn(e);
    }
}