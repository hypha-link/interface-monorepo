import AsyncStorage from "@react-native-async-storage/async-storage";
import { localStreamKey } from "app/components/hooks/useConversationStorage";
import { Conversations } from "app/components/utils/Types";
import { StateIntersect } from ".";
import getOrCreateMessageStream, { ConversationType } from "../Streamr_API";

export async function createMycelium(stateIntersect: StateIntersect, name: string){
    const { ceramicConversations, ceramicStream, streamr, ownProfile, selfId } = stateIntersect;
    const { address: ownAddress } = ownProfile;

    console.log('Create Mycelium ' + name);

    //Check if conversation exists already
    if(!ceramicConversations?.some(conversation => conversation.streamId.includes(name))){
      const stream = await getOrCreateMessageStream(streamr, name, ConversationType.Mycelium, false);
      const newConversation: Conversations = {
        profile: [{ address: name }],
        streamId: stream.id,
        selected: false,
        type: ConversationType.Mycelium,
      }
      //Create a new conversations stream if there are no previously existing streams & pin it
      if(ceramicConversations && ceramicStream){
        await ceramicStream.update({conversations: [...ceramicConversations, newConversation]})
      }
      else{
        const stream = await selfId.client.tileLoader.create(
          {
            conversations: [newConversation]
          },
          {
            tags: ['conversations'],
          },
          {
            pin: true,
          }
        );
        //Add a new local storage record of the stream ID
        await AsyncStorage.setItem(`${ownAddress}-${localStreamKey}`, stream.id.toString());
      }
      return newConversation;
    }
}