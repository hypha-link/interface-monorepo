// ORCHESTRATOR

import { TileDocument } from "@ceramicnetwork/stream-tile";
import useConversationStorage from "app/components/hooks/useConversationStorage";
import { Conversations } from "app/components/utils/Types";
import { DispatchContext, StateContext } from "app/context/state";
import { Actions, GlobalState } from "app/provider/state";
import { useContext } from "react";
import { addConversations } from './addConversations'
import { deleteConversation } from './deleteConversation'
import { selectConversation } from './selectConversation'
import { getValidStream } from './getValidStream'
import { setValidStream } from './setValidStream'
import { setAllValidStreams } from './setAllValidStreams'
import { subscribeToConversations } from './subscribeToConversations'
import { createHyphae } from "./createHyphae";
import { createMycelium } from "./createMycelium"

export type Action<Type, Payload> = {
    type: Type,
    payload?: Payload
}

type AddConversation = Action<'ADD_CONVERSATION', string>
type SelectConversation = Action<'SELECT_CONVERSATION', Conversations>
type DeleteConversation = Action<'DELETE_CONVERSATION', Conversations>
type GetValidStream = Action<'GET_VALID_STREAM', Conversations>
type SetValidStream = Action<'SET_VALID_STREAM', Conversations>
type SetAllValidStreams = Action<'SET_ALL_VALID_STREAMS', Conversations[]>
type SubscribeToConversations = Action<'SUBSCRIBE_TO_CONVERSATIONS',Conversations[]>
type CreateHyphae = Action<'CREATE_HYPHAE', void>
type CreateMycelium = Action<'CREATE_MYCELIUM', string>

export type ActionType = 
| AddConversation
| SelectConversation
| DeleteConversation
| GetValidStream
| SetValidStream
| SetAllValidStreams
| SubscribeToConversations
| CreateHyphae
| CreateMycelium

export type StateIntersect = GlobalState & { ceramicConversations: Conversations[], ceramicStream: TileDocument<Record<string, any>> }

export function useConversations() {
    const state = useContext(StateContext);
    const { ceramicConversations, ceramicStream } = useConversationStorage();
    const stateIntersect = {...state, ceramicConversations, ceramicStream};
    const dispatch = useContext(DispatchContext)

    async function Orchestrator(action: ActionType) {
        switch(action.type){
            case "ADD_CONVERSATION":
                await addConversations(stateIntersect, action.payload).then(newConversation => {
                    dispatch({ type: Actions.ADD_CONVERSATION, payload: newConversation })
                })
                return
            case "SELECT_CONVERSATION":
                dispatch({ type: Actions.SELECT_CONVERSATION, payload: action.payload });
                await selectConversation(stateIntersect, dispatch, action.payload)
                return
            case "DELETE_CONVERSATION":
                await deleteConversation(stateIntersect, action.payload).then(newConversations => {
                    dispatch({ type: Actions.DELETE_CONVERSATION, payload: action.payload })
                })
                return
            case "GET_VALID_STREAM":
                await getValidStream(stateIntersect, action.payload)
                return
            case "SET_VALID_STREAM":
                await setValidStream(stateIntersect, action.payload).then(async (newConversations) => {
                    dispatch({ type: Actions.SET_CONVERSATIONS, payload: newConversations });
                    await ceramicStream.update({conversations: [...newConversations]});
                })
                return
            case "SET_ALL_VALID_STREAMS":
                await setAllValidStreams(stateIntersect, action.payload)
                return
            case "SUBSCRIBE_TO_CONVERSATIONS":
                await subscribeToConversations(stateIntersect, dispatch, action.payload)
                return
            case "CREATE_HYPHAE":
                await createHyphae(stateIntersect).then(newConversation => {
                    dispatch({type: Actions.ADD_CONVERSATION, payload: newConversation});
                })
                return
            case "CREATE_MYCELIUM":
                await createMycelium(stateIntersect, action.payload).then(newConversation => {
                    dispatch({type: Actions.ADD_CONVERSATION, payload: newConversation});
                })
                return
                
        }
    }

    return Orchestrator;
}