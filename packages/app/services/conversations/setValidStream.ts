import { Conversations } from "app/components/utils/Types";
import getConversationProfile from "app/get/getConversationProfile";
import { StateIntersect } from ".";
import { ConversationType } from "../Streamr_API";
import { getValidStream } from "./getValidStream";

export const setValidStream = async (stateIntersect: StateIntersect, _conversation: Conversations) => {
    const { ceramicConversations } = stateIntersect;

    if(_conversation.type !== ConversationType.Hypha) return;

    const validStream = await getValidStream(stateIntersect, _conversation);
    return ceramicConversations.map((conversation) => {
      if(getConversationProfile(_conversation).address === getConversationProfile(conversation).address){
        conversation.streamId = validStream.id;
      }
      return conversation;
    });
}