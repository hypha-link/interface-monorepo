import { Conversations } from "app/components/utils/Types";
import { StateIntersect } from ".";


export async function deleteConversation(stateIntersect: StateIntersect, _conversation: Conversations){
    const { ceramicConversations, ceramicStream } = stateIntersect;

    //Remove conversation if found
    const newConversations = ceramicConversations.filter(conversation => conversation.streamId !== _conversation.streamId);
    await ceramicStream.update({conversations: [...newConversations]});
    return newConversations;
  }