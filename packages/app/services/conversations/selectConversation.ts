import { Conversations } from "app/components/utils/Types";
import { GlobalDispatch } from "app/provider/state";
import { StateIntersect } from ".";
import { subscribeToConversations } from './subscribeToConversations'

 
//Selects a new stream to load when conversation is clicked
export async function selectConversation(stateIntersect: StateIntersect, dispatch: GlobalDispatch, _conversation: Conversations){
    const { conversations } = stateIntersect;
    
    //Subscribe if conversations don't have empty/null streamIds
    if(conversations.filter(conversation => conversation.streamId === "" || conversation.streamId === undefined).length === 0){
        await subscribeToConversations(stateIntersect, dispatch, conversations);
        //Check if user has browser notifications toggled on
        if(Notification.permission === "default"){
        Notification.requestPermission()
        .then((e) => {
            console.log(Notification.permission);
        })
        }
    }
}