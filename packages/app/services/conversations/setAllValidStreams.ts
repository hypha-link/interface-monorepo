import { Conversations } from "app/components/utils/Types";
import { StateIntersect } from ".";
import { setValidStream } from "./setValidStream";

export const setAllValidStreams = async (stateIntersect: StateIntersect, _conversations: Conversations[]) => {
    //Check if any of the conversations have empty or undefined streamIds
    if(_conversations.filter(conversation => conversation.streamId === "" || conversation.streamId === undefined).length > 0){
      Promise.all(_conversations.map(conversation => {
        return new Promise<void>((resolve) => {
          setValidStream(stateIntersect, conversation);
          resolve();
        });
      }));
    }
}